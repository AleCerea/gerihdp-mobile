<?php

define("COUNTRY", "country");

class CountryManager {

    public static $countryDomains = array(
        'geri.it' => 'IT',
        'geri.de' => 'DE',
        'geri.co.uk' => 'UK',
        'geri.fr' => 'FR',
        'geri.es' => 'ES',
        'geri.ro' => 'RO'
    );

    public static function getDomain($countryCode) {
        foreach (self::$countryDomains as $url => $code) {
            if ($code == $countryCode) {
                return $url;
            }
        }
        return "";
    }

    public static function setCountry($code) {
        Yii::app()->session[COUNTRY] = $code;
        // save cookies
    }

    public static function getCountry() {
        $country = "IT";

        if (isset(Yii::app()->session[COUNTRY]))
            $country = Yii::app()->session[COUNTRY];

        return Country::model()->findByPk($country);
    }

    public static function getCountryCode() {
        $key = COUNTRY;
        if (isset(Yii::app()->controller) && isset(Yii::app()->controller->module) && Yii::app()->controller->module->id == "admin") {
            $key = COUNTRY_BO;
        }

        if (isset(Yii::app()->session[$key]))
            return Yii::app()->session[$key];
        else
            return "CO";
    }

    public static function getCountryAvailables() {
        return array(
            "it", "fr", "ro", "uk", "de", "es", "co"
        );
    }

    public static function getCountries() {
        return Country::model()->findAll();
    }

    public static function getLanguages() {
        $country = self::getCountry();

        $languages = array($country->primary_lang);

        if ($country->primary_lang != "en")
            $languages[] = "en";

        if (Yii::app()->controller->action->id == 'page') {
            // check is notizie
            $menu = Yii::app()->request->getQuery('menu');
            $selectedMenu = Menu::model()->localized()->findByLocalizedAttributes(array('url' => $menu, 'country' => CountryManager::getCountryCode()));

            if ($selectedMenu != NULL && $selectedMenu->static == 1 && $selectedMenu->static_type == 'news' && $country->primary_lang != 'en') {
                $languages = array($country->primary_lang);
            }
        }

        return $languages;
    }

    public static function getCountryByDomain() {
        $domain = parse_url(Yii::app()->getBaseUrl(true), PHP_URL_HOST);
        $domain = str_replace('www.', '', $domain);

        if (isset(self::$countryDomains[$domain])) {
            return self::$countryDomains[$domain];
        } else {
            return NULL;
        }

        return null;
    }

}
