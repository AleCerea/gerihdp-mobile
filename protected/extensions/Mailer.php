<?php

class Mailer {

    public static function sendMail($to, $subject, $view, $params = array(), $fromEmail = null) {
        $message = new YiiMailer;

        //this points to the file test.php inside the view path
        $message->setView($view);
        $message->setSubject($subject);

        $message->setTo($to);

        $message->setData($params);
        if ($fromEmail == null)
            $message->setFrom(Yii::app()->params['contact_email'], Yii::app()->params['contact_email_name']);
        else
            $message->setFrom($fromEmail);

        $result = $message->send();

        if (!$result) {
            print_r($message->getError());
        } else
            return true;
    }

}
