
<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FKUploadPhoto
 *
 * @author marcofalcione
 */
class FKUploadPhoto {

    public static function generateFilename($filename, $path = NULL) {

        if ($path === NULL)
            $path = Yii::app()->baseUrl . '../uploads/';

        if (!self::endsWith($path, '/'))
            $path .= '/';

        $retFilename = $filename;
        while (file_exists($path . $retFilename)) {
            $retFilename = rand(111111, 999999) . "_" . $filename;
        }

        return $retFilename;
    }

    public static function endsWith($haystack, $needle) {
        if ($haystack === "")
            return false;

        return substr($haystack, -1) === $needle;
    }

}
