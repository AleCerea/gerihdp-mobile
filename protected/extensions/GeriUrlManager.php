<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GeriUrlManager
 *
 * @author marcofalcione
 */
class GeriUrlManager extends CUrlManager {

    public function createUrl($route, $params = array(), $ampersand = '&') {
        $domain = parse_url(Yii::app()->getBaseUrl(true), PHP_URL_HOST);
        $domain = str_replace('www.', '', $domain);

        unset($params['country']);


        return parent::createUrl($route, $params, $ampersand);
    }

}
