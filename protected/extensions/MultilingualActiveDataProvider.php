<?php

class MultilingualActiveDataProvider extends CActiveDataProvider {

    public $blnLocalize = true;
    public $blnAll = false;

    public function __construct($modelClass, $config = array(), $blnLocalize = true, $blnAll = false) {
        $this->blnLocalize = $blnLocalize;
        $this->blnAll = $blnAll;

        parent::__construct($modelClass, $config);
    }

    protected function fetchData() {
        $criteria = clone $this->getCriteria();
        if (($pagination = $this->getPagination()) !== false) {
            $pagination->setItemCount($this->getTotalItemCount(true));
            $pagination->applyLimit($criteria);
        }
        if (($sort = $this->getSort()) !== false)
            $sort->applyOrder($criteria);

        if ($this->blnLocalize) {
            if ($this->blnAll) {
                return CActiveRecord::model($this->modelClass)->multilingual()->findAll($criteria);
            } else {
                return CActiveRecord::model($this->modelClass)->localized()->findAll($criteria);
            }
        } else {
            return CActiveRecord::model($this->modelClass)->findAll($criteria);
        }
    }

}
