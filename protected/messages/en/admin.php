<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yiic message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE, this file must be saved in UTF-8 encoding.
 */
return array (
  'Layout 1' => '',
  'Layout 2' => '',
  'Layout 3' => '',
  'Layout Box' => '',
  'Layouts' => '',
  'Aggiungi' => 'Add',
  'Aggiungi voce' => 'Add entry',
  'Allegato' => 'Attachment',
  'Annulla' => 'Cancel',
  'Benvenuto!' => 'Welcome!',
  'Blocchi' => 'Blocks',
  'Box' => 'Box',
  'Box 1' => 'Box 1',
  'Box 2' => 'Box 2',
  'Box 3' => 'Box 3',
  'Chiave' => 'Key',
  'Cognome' => 'Last name',
  'Colore di sfondo' => 'Background color',
  'Colori suggeriti' => 'Suggested colors',
  'Contatti' => 'Contacts',
  'Contenuti sito' => 'Site content',
  'Crea' => 'Create',
  'Data' => 'Date',
  'Data Caricamento' => 'Loading date',
  'Data Inserimento' => 'Insert date',
  'Data creazione' => 'Creation date',
  'Dati' => 'Data',
  'Descrizione' => 'Description',
  'Domanda' => 'Question',
  'Elimina' => 'Delete',
  'Elimina Box' => 'Delete Box',
  'Elimina FAQ' => 'Delete FAQ',
  'Elimina Sede' => 'Delete location',
  'Elimina Template' => 'Delete template',
  'Email' => 'Email',
  'Etichetta' => 'Label',
  'Etichetta 1' => 'Label  1',
  'Etichetta 2' => 'Label  2',
  'Etichetta 3' => 'Label  3',
  'Etichetta 4' => 'Label  4',
  'Etichetta 5' => 'Label  5',
  'Etichetta 6' => 'Label  6',
  'FAQ' => 'FAQ',
  'Fai da te' => 'Do it Yourself',
  'Fax' => 'Fax',
  'File' => 'File',
  'Flag' => 'Flag',
  'Gestione Contatti' => 'Contact management',
  'Gestione Dati' => 'Data management',
  'Gestione Highlights' => 'Highlights management',
  'Gestione Menu' => 'Menu management',
  'Gestione News' => 'News Mmanagement',
  'Gestione Pagine' => 'Pages management',
  'Gestione Sedi' => 'Locations management',
  'Gestione Templates' => 'Templates management',
  'Gestione Testi' => 'Texts management',
  'Gestione valori' => 'Values management',
  'Html' => 'Html',
  'Immagine' => 'Image',
  'Indirizzo' => 'Address',
  'Keywords' => 'Keywords',
  'Lingua' => 'Language',
  'Link esterno' => 'External link',
  'Link etichetta 1' => 'Link label 1',
  'Link etichetta 2' => 'Link label 2',
  'Link etichetta 3' => 'Link label 3',
  'Link etichetta 4' => 'Link label 4',
  'Link etichetta 5' => 'Link label 5',
  'Link etichetta 6' => 'Link label 6',
  'Link interno' => 'Internal link',
  'Logout' => 'Logout',
  'Luogo' => 'Place',
  'Map' => 'Map',
  'Menu' => 'Menu',
  'Messaggio' => 'Message',
  'Miniatura' => 'Thumbnail',
  'Modifica' => 'Edit',
  'Modifica Box' => 'Edit box',
  'Modifica FAQ' => 'Edit FAQ',
  'Modifica Sede' => 'Edit location',
  'Modifica Template' => 'Edit template',
  'Modifica Testo' => 'Edit text',
  'Modifica blocco' => 'Edit block',
  'Modifica pagina' => 'Edit page',
  'Modifica sede' => 'Edit location',
  'Modifica template' => 'Edit template',
  'Modifica valore' => 'Edit value',
  'Multimedia' => 'Multimedia',
  'Nazione' => 'Country',
  'News' => 'News',
  'No' => 'No',
  'Nome' => 'Name',
  'Non Impostato' => 'Not set',
  'Non visibile' => 'Not visible',
  'Nuova' => 'New',
  'Nuova FAQ' => 'New FAQ',
  'Nuova News' => 'New news',
  'Nuova Pagina' => 'New page',
  'Nuova Sede' => 'New location',
  'Nuova pagina' => 'New page',
  'Nuova sede' => 'New location',
  'Nuovo' => 'New',
  'Nuovo Box' => 'New box',
  'Nuovo Template' => 'New template',
  'Nuovo blocco' => 'New block',
  'Nuovo box' => 'New box',
  'Nuovo file' => 'New file',
  'Nuovo template' => 'New template',
  'Nuovo testo' => 'New text',
  'Nuovo valore' => 'New value',
  'Ordine' => 'Order',
  'Pagina' => 'Page',
  'Pagine' => 'Pages',
  'Risposta' => 'Answer',
  'Salva' => 'Save',
  'Search' => 'Search',
  'Sedi' => 'Locations',
  'Sei sicuro di voler eliminare questo blocco?' => 'Are you sure you want to delete this block?',
  'Sei sicuro?' => 'Are you sure?',
  'Si' => 'Yes',
  'Slider' => 'Slider',
  'Societa' => 'Company',
  'Statica' => 'Statistic',
  'Telefono' => 'Phone',
  'Templates' => 'Templates',
  'Testi' => 'Texts',
  'Testi Box' => 'Box texts',
  'Testo' => 'Text',
  'Testo Introduttivo' => 'Introduction',
  'Tipo' => 'Type',
  'Tipo statico' => 'Static type',
  'Titolo' => 'Title',
  'URL' => 'URL',
  'Valore' => 'Value',
  'Valore ' => 'Value ',
  'Valori' => 'Values',
  'Vedi/Modifica blocchi' => 'View / Edit blocks',
  'Visibile' => 'Visible',
  'Visualizza' => 'Show',
  'Visualizza FAQ' => 'Show FAQ',
  'Visualizza Sede' => 'Show location',
  'Visualizza Template' => 'Show template',
  'Visualizza pagina' => 'Show page',
  'Voce di Menu' => 'Menu entry',
  'codice' => 'Code',
  'cognome' => 'Last Name',
  'e-mail' => 'e-mail',
  'messaggio' => 'Message',
  'nome' => 'Name',
  'società' => 'Company',
  'telefono' => 'Phone',
);
