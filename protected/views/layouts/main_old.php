<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <meta name="language" content="en">

                <!-- PERSONAL CSS -->
                <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/media_queries.css" />
                <link rel='stylesheet' type="text/css" href="<?php echo Yii::app()->request->baseUrl ?>/css/colors.css" />
                <link rel='stylesheet' type="text/css" href="<?php echo Yii::app()->request->baseUrl ?>/css/band.css" />
                <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />

                <!-- blueprint CSS framework -->
                <!--link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection"-->
                <!--link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print"-->
                <!--[if lt IE 8]>
                <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
                <![endif]-->

        <!--link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
        <!--link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css"-->

                <!-- BOOTSTRAP -->
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">
                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>


                        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
                        </head>

                        <body>

                            <div class="container-fluid" id="page">
                                <div id="header">
                                    <div class="content">
                                        <div class="row-fluid">
                                            <div class="col-xs-6">
                                                <div id="header-images">
                                                    <a href="<?Php echo Yii::app()->baseUrl; ?>">
                                                        <?Php echo CHtml::image(Yii::app()->request->baseUrl . '/images/logo.png', "logo", array('id' => 'logo')); ?><br/>
                                                        <?Php echo CHtml::image(Yii::app()->request->baseUrl . '/images/payoff.png', "payoff", array('id' => 'payoff')); ?>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div id="social_menu" class="float_right">
                                                    <?Php echo CHtml::link(CHtml::image(Yii::app()->request->baseUrl . '/images/icon_linkedin.png', "linkedIn_logo", array('class' => "social_image")), Config::getConfigByKey("inurl"), array('target' => '_blank')); ?>
                                                    <?Php echo CHtml::link(CHtml::image(Yii::app()->request->baseUrl . '/images/icon_facebook.png', "Facebook_logo", array('class' => "social_image")), Config::getConfigByKey("fburl"), array('target' => '_blank')); ?>
                                                    <?Php echo CHtml::link(CHtml::image(Yii::app()->request->baseUrl . '/images/icon_twitter.png', "Twitter_logo", array('class' => "social_image")), Config::getConfigByKey("twurl"), array('target' => '_blank')); ?>
                                                </div>
                                                <div id="nazione_select" class="float_right">
                                                    <div id="nazione_button" class="float_left">
                                                        <?php echo CountryManager::getCountry()->name ?>
                                                    </div>
                                                    <img src="<?php echo Yii::app()->baseUrl ?>/images/arrow_down.png" />
                                                    <div id="nazione_select_list">
                                                        <?php foreach (CountryManager::getCountries() as $country) { ?>
                                                            <a href="<?php echo $this->createUrl('site/changeCountry', array('country' => $country->code)) ?>">
                                                                <div class="country_item">
                                                                    <?php echo $country->name ?>
                                                                </div>
                                                            </a>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>		
                                                <div id="header_phone" class="float_right">	        			
                                                    <span class="glyphicon glyphicon-earphone"></span>
                                                    <span><?php echo Config::getConfigByKey("telefono") ?></span>
                                                    <div class="clear"></div>
                                                </div>
                                                <?php
                                                $languages = CountryManager::getLanguages();

                                                if (count($languages) > 1) {
                                                    ?>
                                                    <div id="languages" class="float_left">
                                                        <?php foreach ($languages as $lang) { ?>
                                                            <a href="<?php echo $this->createUrl('', array('lang' => $lang)) ?>">
                                                                <div class="lang float_left <?php echo (Yii::app()->language == $lang) ? "active" : "" ?>">
                                                                    <?php echo strtoupper($lang) ?>
                                                                </div>
                                                            </a>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- header -->

                                <?php if (isset($this->breadcrumbs)): ?>
                                    <?php $this->widget('zii.widgets.CBreadcrumbs', array('links' => $this->breadcrumbs,)); ?><!-- breadcrumbs -->
                                <?php endif ?>

                                <div id="mainmenu">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading0">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" href="<?php echo Yii::app()->homeUrl ?>"> <?php echo Yii::t("site", "HOME"); ?></a>
                                                </h4>
                                            </div>
                                            <div id="collapse0" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading0">
                                                <!--div class="panel-body">
                                                </div-->
                                            </div>
                                        </div>
                                        <?php
                                        $counter = 0;
                                        foreach (Menu::getItems() as $menuItem) {
                                            $counter++;
                                            $pages = $menuItem->getPages();
                                            ?>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading<?Php echo $counter ?>">
                                                    <h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?Php echo $counter ?>" aria-expanded="false" aria-controls="collapse<?Php echo $counter ?>"> <?php echo strtoupper($menuItem->label);
                                            ?><img class="menu-icon" alt="plus" src="<?Php echo Yii::app()->baseUrl; ?>/images/qmas.png"> </a></h4>
                                                </div>
                                                <div id="collapse<?Php echo $counter ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?Php echo $counter ?>">
                                                    <?php foreach ($menuItem->getPages() as $num => $page) {
                                                        ?>
                                                        <div class="">
                                                            <a href="<?php echo $this->createUrl('/site/page', array('country' => strtolower(CountryManager::getCountryCode()), 'menu' => $menuItem->url, 'page' => $page->url)) ?>" style="text-decoration: underline">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading" style="padding-top: 0px !important">
                                                                        <div class="submenu">
                                                                            <?php echo $page->title
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div> </a>
                                                            <?php foreach ($page->getBlocks() as $block) {
                                                                ?>
                                                                <a href="<?php echo $this->createUrl('/site/page', array('country' => strtolower(CountryManager::getCountryCode()), 'menu' => $menuItem->url, 'page' => $page->url)) ?>#<?php echo $block->url ?>">
                                                                    <div style="margin-left : 50px;">
                                                                        <?php echo $block->title
                                                                        ?>
                                                                    </div> </a>
                                                            <?php }
                                                            ?>
                                                        </div>
                                                    <?php }
                                                    ?>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                        <?Php } ?>

                                    </div><!-- mainmenu -->

                                    <?php echo $content; ?>

                                    <div class="clear"></div>

                                    <div id="footer">
                                        <strong>Copyright &copy; <?php echo date('Y'); ?> GERI HDP<br></strong>
                                        Via Lago di Nemi 25, Milano - Capitale Sociale: 100.000, 00 - P.IVA: 05953840963 - Iscritto al registro delle Imprese di Milano, REA 1861391
                                        <br/><strong>Revisione Website Ver. 7 del 01/2015</strong>
                                        <br/>Credits:<a target="_blank" href="http://www.ipomea.it" style="color: #fff !important;">  www.ipomea.it</a>
                                    </div><!-- footer -->

                                </div><!-- page -->
                                <?php Yii::app()->bootstrap->register(); ?>
                        </body>
                        </html>
