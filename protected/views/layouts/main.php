<?php
$domain = parse_url(Yii::app()->getBaseUrl(true), PHP_URL_HOST);
$domain = str_replace('www.', '', $domain);


if ($domain == 'paiementenligne.geri.fr') {
    $domain = 'geri.fr';
} else if ($domain == 'pagaonline.geri.it') {
    $domain = 'geri.it';
}

if (isset(CountryManager::$countryDomains[$domain])) {
    $geriMainUrl = "http://$domain";
} else {
    $geriMainUrl = "http://gerihdp.com";
}

$cByDomain = CountryManager::getCountryByDomain();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="initial-scale=1, maximum-scale=1" />
        <meta name="language" content="it" />

        <!-- BOOTSTRAP CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css" />

        <!-- PERSONAL CSS -->
        <link rel='stylesheet' type="text/css" href="<?php echo Yii::app()->request->baseUrl ?>/css/colors.css" />
        <link rel='stylesheet' type="text/css" href="<?php echo Yii::app()->request->baseUrl ?>/css/band.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />


        <!-- BOOTSTRAP JS -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>

    <body>

        <div class="container-fluid" id="page">
            <div id="header">
                <div class="content">
                    <div class="row-fluid">
                        <div class="col-xs-6">
                            <div id="header-images">
                                <a href="<?Php echo Yii::app()->homeUrl; ?>">
                                    <?php if ($cByDomain === NULL) { ?>
                                        <?Php echo CHtml::image(Yii::app()->request->baseUrl . '/images/logo_gerihdp.png', "logo", array('id' => 'logo')); ?><br/>
                                    <?php } else { ?>
                                        <?Php echo CHtml::image(Yii::app()->request->baseUrl . '/images/logo.png', "logo", array('id' => 'logo')); ?><br/>
                                    <?php } ?>
                                    <?Php echo CHtml::image(Yii::app()->request->baseUrl . '/images/payoff_' . Yii::app()->language . '.png', "payoff", array('id' => 'payoff')); ?>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div id="header-right-side" style=" height:100px; text-align: right;">
                                <div id="social_menu" style="display: block; float: right; width : 100%">
                                    <?php
                                    $inUrl = Config::getConfigByKey("inurl");
                                    $twUrl = Config::getConfigByKey("twurl");
                                    $fbUrl = Config::getConfigByKey("fburl");

                                    if ($inUrl != null && $inUrl != "") {
                                        ?>
                                        <?Php echo CHtml::link(CHtml::image(Yii::app()->request->baseUrl . '/images/icon_linkedin.png', "linkedIn_logo", array('class' => "social_image")), Config::getConfigByKey("inurl"), array('target' => '_blank')); ?>
                                    <?php } ?>
                                    <?php if ($twUrl != null && $twUrl != "") { ?>
                                        <?Php echo CHtml::link(CHtml::image(Yii::app()->request->baseUrl . '/images/icon_twitter.png', "Twitter_logo", array('class' => "social_image")), Config::getConfigByKey("twurl"), array('target' => '_blank')); ?>
                                    <?php } ?>
                                    <?php if ($fbUrl != null && $fbUrl != "") { ?>
                                        <?Php echo CHtml::link(CHtml::image(Yii::app()->request->baseUrl . '/images/icon_facebook.png', "Facebook_logo", array('class' => "social_image")), Config::getConfigByKey("fburl"), array('target' => '_blank')); ?>
                                    <?php } ?>


                                </div>	
                                <?php
                                if ($cByDomain === NULL) {
                                    ?>                                
                                    <div id="nazione_select" style="display: block; float: right; width : 100%; background: #FFF;border-radius: 0px;">
                                        <select style="width: 100% !important;" onchange="location = this.options[this.selectedIndex].value;">
                                            <option disabled selected>GERI HDP</option>
                                            <?php
                                            foreach (CountryManager::getCountries() as $country) {
                                                if ($country->visible == 0)
                                                    continue;
                                                ?>
                                                <option class="country_item" value="http://<?php echo CountryManager::getDomain($country->code); ?>">
                                                    <?php echo $country->name ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                <?php } else { ?>
                                    <div id="nazione_select" style="display: block; float: right; width : 100%;background: #FFF;border-radius: 0px;">
                                        <select style="width: 100% !important;" onchange="location = this.options[this.selectedIndex].value;">
                                            <option value="http://gerihdp.com">GERI HDP</option>
                                            <?php
                                            foreach (CountryManager::getCountries() as $country) {
                                                if ($country->visible == 0)
                                                    continue;
                                                ?>
                                                <option class="country_item" value="http://<?php echo CountryManager::getDomain($country->code); ?>" <?php
                                                if (CountryManager::getCountry()->name == $country->name) {
                                                    echo "selected = selected";
                                                }
                                                ?> >
                                                            <?php echo $country->name ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                <?php } ?>
                                <div style="display: block; float: right; width : 100%">
                                    <div>	        			
                                        <span class="glyphicon glyphicon-earphone"></span>
                                        <span style = "font-weight: 700;color: #717171;font-size: 13px;text-decoration: underline;position: relative;">
                                            <?php echo Config::getConfigByKey("telefono") ?>
                                        </span>
                                        <br/>
                                        <?php if (CountryManager::getCountryCode() == "IT") { ?>
                                            <small style="position:relative;top:0px;line-height: 10px;">Numero verde<br/>commerciale</small>
                                        <?php } ?>
                                        <div class="clear"></div>
                                    </div>       			
                                </div>
                                <div>
                                    <?php $languages = CountryManager::getLanguages(); ?>
                                    <?php if (count($languages) > 1) { ?>
                                        <div id="languages" class="float_right">
                                            <?php foreach ($languages as $lang) { ?>
                                                <a href="?lang=<?php echo $lang ?>">
                                                    <div class="lang float_left <?php echo (Yii::app()->language == $lang) ? "active" : "" ?>">
                                                        <?php echo strtoupper($lang) ?>
                                                    </div>
                                                </a>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div><!-- header -->

            <?php if (isset($this->breadcrumbs)): ?>
                <?php $this->widget('zii.widgets.CBreadcrumbs', array('links' => $this->breadcrumbs,)); ?><!-- breadcrumbs -->
            <?php endif ?>

            <div id="mainmenu">
                <script src="//code.jquery.com/jquery-1.10.2.js"></script>
                <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
                <script type="text/javascript">
                                            $(function () {
                                                $("#accordion").accordion({
                                                    collapsible: true,
                                                    active: false,
                                                    heightStyle: "content",
                                                    beforeActivate: function (event, ui) {
                                                        if ($(ui.newPanel.context).hasClass('no-accordion')) {
                                                            event.preventDefault();

                                                            var target = $(ui.newPanel.context).find('a').attr('target');
                                                            var url = $(ui.newHeader.context).find('a').attr('href');
                                                            if (target == '_blank') {
                                                                var win = window.open(url, '_blank');
                                                                win.focus();
                                                            } else {
                                                                window.location = url;
                                                            }

                                                        }
                                                    }
                                                });
                                            });
                </script>
                <div class="panel-group" style="margin-bottom:0px;">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="<?php echo Yii::app()->homeUrl ?>"> 
                                <h4 style="margin:0px;"><?php echo strtoupper(Yii::t("site", "HOME")); ?></h4>
                            </a>
                        </div>
                    </div>
                </div>
                <div id="accordion">
                    <?php
                    $countryCode = CountryManager::getCountryCode();
                    foreach (Menu::getItems() as $menuItem) {
                        if ($menuItem->static_type == 'news' && $countryCode != 'UK' && Yii::app()->language == 'en') {
                            continue;
                        }

                        $pages = $menuItem->getPages();
                        ?>
                        <div class="panel-group <?php echo count($pages) == 0 ? 'no-accordion' : '' ?>" style="margin-bottom:0px;">
                            <div class="panel panel-default" >
                                <div class="panel-heading">
                                    <?php if (count($pages) == 0) { ?>
                                        <a href="<?php echo $this->createUrl('/site/page', array('country' => strtolower(CountryManager::getCountryCode()), 'menu' => $menuItem->url, 'page' => '')) ?>">
                                        <?php } else { ?>
                                            <a href="#">
                                            <?php } ?>
                                            <h4 style="margin:0px;"><?php echo strtoupper($menuItem->label); ?></h4>
                                        </a>
                                </div>
                            </div>
                        </div>

                        <div>
                            <?php if (count($pages) > 0) { ?>
                                <?php foreach ($menuItem->getPages() as $num => $page) { ?>
                                    <div class="panel panel-default" style="margin-bottom: 2px;">
                                        <div class="panel-heading" style="padding-top: 0px !important">
                                            <div class="submenu">
                                                <a href="<?php echo $this->createUrl('/site/page', array('country' => strtolower(CountryManager::getCountryCode()), 'menu' => $menuItem->url, 'page' => $page->url)) ?>" style="text-decoration: underline">
                                                    <?php echo $page->title; ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php foreach ($page->getBlocks() as $block) { ?>
                                        <a href="<?php echo $this->createUrl('/site/page', array('country' => strtolower(CountryManager::getCountryCode()), 'menu' => $menuItem->url, 'page' => $page->url)) ?>#<?php echo $block->url ?>">
                                            <div style="padding-left : 50px; background-color:#fff;">
                                                <?php echo $block->title; ?>
                                            </div>
                                        </a>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        </div>

                        <?php if ($menuItem->static_type == 'news' && CountryManager::getCountryCode() == 'IT' && Yii::app()->language == 'it') { ?>
                            <div class="panel-group <?php echo count($pages) == 0 ? 'no-accordion' : '' ?>" style="margin-bottom:0px;">
                                <div class="panel panel-default" >
                                    <div class="panel-heading">
                                        <a href="http://blog.gerihdp.com/" target="_blank">
                                            <h4 style="margin:0px;">BLOG</h4>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div>

                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
                <?php
                $countryCode = CountryManager::getCountryCode();
                if ($countryCode != 'CO' && $countryCode != 'UK' && $countryCode != 'ES') {
                    ?>
                    <div class="panel-group" style="margin-bottom:0px;">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <?php
                                $urls = array(
                                    'faidate_url_it' => 'http://pagaonline.geri.it',
                                    'faidate_url_en' => 'http://paymentgateway.geri.it',
                                    'faidate_url_fr' => 'http://paiementenligne.geri.fr'
                                );
                                $lang = Yii::app()->language;
                                if (!isset($urls['faidate_url_' . $lang])) {
                                    $fdtUrl = "$geriMainUrl/faidate/default/index";
                                } else {
                                    $fdtUrl = $urls['faidate_url_' . $lang];
                                }
                                ?>

                                <a href="<?php echo $fdtUrl ?>"> 
                                    <h4 style="margin:0px;"><?php echo strtoupper(Yii::t("site", "Fai da te")); ?></h4>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div><!-- mainmenu -->

            <?php echo $content; ?>

            <div class="clear"></div>

            <div id="footer">
                <?php echo Text::getTextByKey("Footer"); ?>
                <br/>Credits: <a target="_blank" href="http://www.ipomea.it" style="color: #fff !important;">ipomea.it</a>
            </div><!-- footer -->
            <?php $this->renderPartial("//layouts/_cookie_bar") ?>

            <?php
            $lang = strtolower(CountryManager::getCountryCode());
            ?>
            <?php if ($lang == 'es') { ?>
                <!-- Global site tag (gtag.js) - Google Analytics -->
                <script async src="https://www.googletagmanager.com/gtag/js?id=UA-29899890-3"></script>
                <script>
                                                        window.dataLayer = window.dataLayer || [];
                                                        function gtag() {
                                                            dataLayer.push(arguments);
                                                        }
                                                        gtag('js', new Date());

                                                        gtag('config', 'UA-29899890-3');
                </script>
            <?php } else { ?>
                <!-- Analytics -->
                <script>
                    (function (i, s, o, g, r, a, m) {
                        i['GoogleAnalyticsObject'] = r;
                        i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                        a = s.createElement(o),
                                m = s.getElementsByTagName(o)[0];
                        a.async = 1;
                        a.src = g;
                        m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                    ga('create', 'UA-52187936-1', 'auto');
                    ga('send', 'pageview');

                </script>
            <?php } ?>

        </div><!-- page -->
        <?php //Yii::app()->bootstrap->register();     ?>
    </body>
</html>
