<!-- Cookies -->
<?php if (!isset($_COOKIE['cookie_bar'])) { ?>
    <script type="text/javascript">

        function closeCookies() {
            $.ajax({
                url: '<?php echo $this->createUrl('site/setCookies') ?>',
            });
            $('#cookie-bar').remove();
        }

    </script>
    <style type="text/css">

        #cookie-bar {
            position: fixed;
            width: 100%;
            bottom: 0px;
            left: 0px;
            background: #FFF;
            z-index: 99999999;
            border-top: 2px solid #f27631;
        }

        #cookie-content {
            padding: 10px;
        }

        #accept-button {
            display: inline-block;
            text-align: right;
            text-decoration: none;
            margin-left: 15px;
            background: #f27631;
            padding: 3px 17px;
            border: 1px solid #f27631;
            -webkit-border-radius: 5px 5px 5px 5px;
            border-radius: 5px 5px 5px 5px;
            color: #FFF !important;
        }

        a {
            color: #000;
            text-decoration: underline; 
        }
    </style>
    <div id="cookie-bar">
        <div id="cookie-content">
            <?php
            $lang = Yii::app()->language;
            $country = CountryManager::getCountryCode();

            if ($country == "CO") {
                if ($lang == "it")
                    $url = "http://gerihdp.com/governance/privacy#privacy-policy-per-i-visitatori-del-sito";
                else
                    $url = "http://www.gerihdp.com/governance/privacy#privacy-policy-for-website-users";
            }
            else if ($country == "IT") {
                if ($lang == "it")
                    $url = "http://geri.it/governance/privacy#privacy-policy-per-i-visitatori-del-sito";
                else
                    $url = "http://www.geri.it/governance/privacy#privacy-policy-for-website-users";
            }
            else if ($country == "RO") {
                if ($lang == "ro")
                    $url = "http://geri.ro/governance/privacy#politica-de-confidențialitate-pentru-vizitatorii-site-ului";
                else
                    $url = "http://geri.ro/governance/privacy#privacy-policy-for-website-users";
            } else if ($country == "FR") {
                if ($lang == "fr")
                    $url = "http://geri.fr/gouvernance/vie-privee#politique-de-confidentialité-pour-les-visiteurs-du-site";
                else
                    $url = "http://geri.fr/governance/privacy#privacy-policy-for-website-users";
            }
            else if ($country == "DE") {
                if ($lang == "de")
                    $url = "http://geri.de/Geschaeftsfuehrung/datenschutz#datenschutzerklärung";
                else
                    $url = "http://geri.de/governance/privacy#privacy-policy";
            }
            else if ($country == "ES") {
                if ($lang == "es")
                    $url = "http://geri.es/governance/privacidad#política-de-privacidad-para-los-visitantes-del-sitio";
                else
                    $url = "http://geri.es/governance/privacy#privacy-policy-for-website-users";
            }
            else if ($country == "UK") {
                $url = "http://geri.co.uk/governance/privacy#privacy-policy-for-website-users ";
            }
            ?>
            <?php echo Yii::t("site", "Questo sito utilizza i cookies per monitorare l'attività web del sito. Per avere ulteriori informazioni, consulta la nostra") ?> <a href="<?php echo $url ?>"><?php echo Yii::t("site", "Privacy Policy") ?></a>


            <a id="accept-button" href="javascript:;" onclick="closeCookies()"><?php echo Yii::t("site", "Accetto") ?></a>
        </div>
    </div>
<?php } ?>