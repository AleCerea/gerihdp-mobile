<?php
/* @var $this SiteController */

//$this->pageTitle=Yii::app()->name; MAYBE NOT NEEDED
?>
<div class="back_footer_boxes clearfix">
    <div id="seo_boxes">
        <?php
        foreach (range(1, 4) as $i) {
            $seoBox = Seobox::model()->localized()->findByAttributes(array('box' => 'box' . $i, 'nazione' => CountryManager::getCountryCode()));

            if ($seoBox != null && $seoBox->visibile == 1) {
                $url = $seoBox->getURL();
                $urlTarget = $seoBox->getURLTarget();

                if ($url != NULL && $url != "") {
                    ?>
                    <a href="<?php echo $url ?>" target="<?php echo $urlTarget ?>">
                    <?php } ?>

                    <div class = "box">
                        <?php if ($i == 1) { ?>
                            <h1><?php echo $seoBox->titolo ?></h1>
                        <?php } else { ?>
                            <h2><?php echo $seoBox->titolo ?></h2>
                        <?php } ?>
                        <div id="seo_box_<?= $i ?>" class="box_content layout_1_box">
                            <div>
                                <img src="<?= $seoBox->getImage() ?>" alt="<?= $seoBox->image_alt ?>" title="<?= $seoBox->image_alt ?>" />
                                <p class="">
                                    <?php echo $seoBox->testo ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php if ($url != NULL && $url != "") { ?>
                    </a>
                    <?php
                }
            }
        }
        ?>
    </div>
</div>

<div class="back_footer_boxes">
    <div id = "footer_boxes"><?php
        if (CountryManager::getCountryCode() == "IT") {
            $name = "boxS";
            $data = $$name;
            ?><div class = "box" style="cursor: default;">
                <h3><?php echo Text::getTextByKey("Box Recapiti Home - Titolo", true) ?>
                    <div class = "clear"></div>
                </h3>
                <div class="box_content normal_box">
                    <ul class="slides">
                        <ul class="slides">
                            <?php foreach ($data as $box) { ?>
                                <li title="<?php echo $box->getURL() ?>" target="<?php echo $box->getURLTarget() ?>">
                                    <div>
                                        <?php if ($box->immagine != null && $box->testo != null) { ?>
                                            <img src = "<?php echo $box->getImage() ?>" alt = "" />
                                            <p class = "box_italic">
                                                <?php echo $box->testo ?>
                                            </p>
                                        <?php } else if ($box->testo == null) { ?>
                                            <img src = "<?php echo $box->getImage() ?>" alt = "" class="full" />
                                        <?php } else if ($box->immagine == null) {
                                            ?>
                                            <p class = "box_italic">
                                                <?php echo $box->testo; ?>
                                            </p>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </ul>
                </div>
            </div><?php
        }
        $layoutBox[1] = ConfigValues::model()->findByAttributes(array('key' => 'layoutbox1'));
        $layoutBox[2] = ConfigValues::model()->findByAttributes(array('key' => 'layoutbox2'));
        $layoutBox[3] = ConfigValues::model()->findByAttributes(array('key' => 'layoutbox3'));

        foreach (range(1, 3) as $numBox) {
            $layout = $layoutBox[$numBox]->valore;
            $name = "box$numBox";
            $data = $$name;

            if (count($data) == 0) {
                continue;
            }

            if ($layout == 1) {
                ?><div class="box">
                    <h3><?php echo Text::getTextByKey("Box $numBox Home - Titolo", true) ?></h3>
                    <div id="box<?php echo $numBox ?>" class="box_content layout_1_box">
                        <ul class="slides">
                            <?php foreach ($data as $box) { ?>
                                <li title="<?php echo $box->getURL() ?>" target="<?php echo $box->getURLTarget() ?>">
                                    <div>
                                        <?php if ($box->immagine != null && $box->testo != null) { ?>
                                            <img src = "<?php echo $box->getImage() ?>" alt = "" />
                                            <p class = "box_italic">
                                                <?php echo $box->testo ?>
                                            </p>
                                        <?php } else if ($box->testo == null) { ?>
                                            <img src = "<?php echo $box->getImage() ?>" alt = "" class="full" />
                                        <?php } else if ($box->immagine == null) {
                                            ?>
                                            <p class = "box_italic">
                                                <?php echo $box->testo; ?>
                                            </p>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="box_hover">
                        <img src = "<?php echo Yii::app()->baseUrl ?>/images/home/box_plus_big.png" alt = "" />
                    </div>
                </div><?php } else if ($layout == 2) { ?><div class="box">
                    <h3><?php echo Text::getTextByKey("Box $numBox Home - Titolo", true) ?></h3>
                    <div id="box<?php echo $numBox ?>" class = "box_content normal_box">
                        <ul class="slides">
                            <?php
                            for ($i = 0; $i < count($data); $i = $i + 2) {
                                $box = $data[$i];
                                ?>
                                <li>
                                    <a href="<?php echo $box->getURL() ?>" target="<?php echo $box->getURLTarget() ?>">
                                        <div class="box_row">
                                            <div class="table">
                                                <div class="image">
                                                    <?php if ($box->immagine != null) { ?>
                                                        <img src = "<?php echo $box->getImage() ?>" alt = "" />
                                                    <?php } ?>
                                                </div>
                                                <div class="arrow">
                                                    <img src="<?php echo Yii::app()->baseUrl ?>/images/home/arrow_row_box.png" />
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="clear"></div>

                                    <?php
                                    if (isset($data[$i + 1])) {
                                        $box = $data[$i + 1];
                                        ?>
                                        <a href="<?php echo $box->getURL() ?>" target="<?php echo $box->getURLTarget() ?>">
                                            <div class="box_row">
                                                <div class="table">
                                                    <div class="image">
                                                        <?php if ($box->immagine != null) { ?>
                                                            <img src = "<?php echo $box->getImage() ?>" alt = "" />
                                                        <?php } ?>
                                                    </div>
                                                    <div class="arrow">
                                                        <img src="<?php echo Yii::app()->baseUrl ?>/images/home/arrow_row_box.png" />
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="clear"></div>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div><?php } else if ($layout == 3) { ?><div class = "box">
                    <h3><?php echo Text::getTextByKey("Box $numBox Home - Titolo", true) ?>
                        <div class = "clear"></div>
                    </h3>
                    <div id="box<?php echo $numBox ?>" class="box_content normal_box">
                        <ul class="slides">
                            <?php foreach ($data as $box) { ?>
                                <li>
                                    <div class="table">
                                        <div class="table-cell">
                                            <a href="<?php echo $box->getURL() ?>" target="<?php echo $box->getURLTarget() ?>">
                                                <img src = "<?php echo $box->getImage() ?>" alt = "image" class="full" />
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            <?php }
                            ?>
                        </ul>
                    </div>
                </div>
                <?php
            }
        }
        ?>
        <div class = "clear"></div>
    </div>
</div>

<script defer src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.flexslider-min.js"></script>
<script type="text/javascript">
    var urls = [];
    var box_actual_slide = {
        "box1": 0,
        "box2": 0,
        "box3": 0,
    };

    var box1urls = [];
<?php foreach ($box1 as $b) { ?>
        box1urls.push({
            url: "<?php echo $b->getURL() ?>",
            target: "<?php echo $b->getURLTarget() ?>",
        });
<?php } ?>

    var box2urls = [];
<?php foreach ($box2 as $b) { ?>
        box2urls.push({
            url: "<?php echo $b->getURL() ?>",
            target: "<?php echo $b->getURLTarget() ?>",
        });
<?php } ?>

    var box3urls = [];
<?php foreach ($box3 as $b) { ?>
        box3urls.push({
            url: "<?php echo $b->getURL() ?>",
            target: "<?php echo $b->getURLTarget() ?>",
        });
<?php } ?>

    urls = {
        "box1": box1urls,
        "box2": box2urls,
        "box3": box3urls
    };

    $(function () {
        $('.normal_box').flexslider({
            animation: "slide",
            directionNav: false,
            controlNav: false,
            touch: false,
            direction: 'vertical',
        });

        $('.layout_1_box').flexslider({
            animation: "slide",
            directionNav: false,
            controlNav: false,
            touch: false,
            direction: 'vertical',
            after: function (slider) {
                var boxId = $(slider.context).attr('id');
                box_actual_slide[boxId] = slider.currentSlide;
            }
        });
        $('.box_hover').click(function () {
            var boxID = $(this).siblings(".box_content").first().attr("id");
            var objUrl = urls[boxID][box_actual_slide[boxID]];
            if (objUrl.target == "") {
                window.location = objUrl.url;
            } else {
                window.open(objUrl.url, objUrl.target);
            }
        });
    });
</script>

