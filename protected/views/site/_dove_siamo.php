<div id="dove-siamo">
    <div class="band s1 c-white m767">
        <div class="band-inner">
            <?php
            foreach ($sedi as $sede) {
                $countryCode = "";
                $flag = $sede->flag;
                if ($flag == "ita") {
                    $countryCode = 'IT';
                } else if ($flag == 'fra') {
                    $countryCode = 'FR';
                } else if ($flag == 'eng') {
                    $countryCode = 'UK';
                } else if ($flag == 'deu') {
                    $countryCode = 'DE';
                } else if ($flag == 'rom') {
                    $countryCode = 'RO';
                } else if ($flag == 'spa') {
                    $countryCode = 'ES';
                }

                $domain = CountryManager::getDomain($countryCode);
                ?>
                <div class="drawer" onclick="location = 'http://<?= $domain ?>';" style="cursor: pointer;">
                    <div class="box c-gray">
                        <div class="hgroup">
                            <h1>
                                <img class="right" src="<?php echo Yii::app()->request->baseUrl ?>/images/<?php echo $sede->flag ?>.jpg">
                                <?php echo $sede->titolo ?>
                            </h1>
                        </div>
                        <div class="drawer-body">
                            <div class="drawer-img fullWidth">
                                <span>
                                    <img src="<?php echo $sede->getImageThumb() ?>">
                                </span>
                            </div>
                            <div class="drawer-txt">
                                <p><?php echo Yii::t("site", "Telefono") ?> <?php echo $sede->telefono ?></p>
                                <p><?php echo Yii::t("site", "Fax") ?> <?php echo $sede->fax ?></p>
                                <p>
                                    <?php echo nl2br($sede->indirizzo) ?>
                                </p>
                            </div>
                            <div class="drawer-map">
                                <?php echo $sede->map ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>