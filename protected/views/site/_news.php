<script type="text/javascript">

    $(function () {
        $('#category_filter').change(function () {
            var categoria = $(this).val();
            $.fn.yiiListView.update("news_list", {data: {'categoria': categoria}})
        });
    });

</script>
<?php
$categories = CategoriaNews::model()->localized()->findAllByAttributes(array('country' => CountryManager::getCountryCode()), array('order' => 't.nome'));
if (count($categories) > 0) {
    ?>
    <div class="categories">
        <span><?php echo Yii::t('site', 'Filtra per categoria:') ?></span>
        <select id="category_filter">
            <option></option>
            <?php foreach ($categories as $c) { ?>
                <option value="<?= $c->id ?>"><?= $c->nome ?></option>
            <?php } ?>
        </select>
    </div>
<?php } ?>
<div class="news_list">
    <?php
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $model->searchForList(),
        'itemView' => '_news_item',
        'viewData' => array(
            'menuItem' => $menuItem
        ),
        'summaryText' => false
    ));
    ?>
</div>