<a href="<?php echo $this->createUrl('site/page', array('country' => strtolower(CountryManager::getCountryCode()), 'menu' => $menuItem->url, "page" => $data->url)) ?>">
    <div class="news_item" style="background-color:#fff; margin:5px; padding: 5px;">
        <div class="photo">
            <img src="<?php echo $data->getImageThumb() ?>" alt="" />
        </div>
        <style>.items > a:hover { text-decoration : none;} </style>
        <div class="news_brief_content">
            <h1><?php echo $data->titolo ?></h1>
            <small><?php echo date("d/m/Y", strtotime($data->data)) ?> - <?php echo $data->getTipologia() ?> 
                <?php if ($data->categoria != NULL && $data->categoria != "" && $data->categoria != 0) { ?> - <?php echo $data->getCategory() ?><?php } ?>
            </small>
            <p><?php echo $data->getInitialText() ?></p>
        </div>
        <div class="clear"></div>
    </div>
</a>