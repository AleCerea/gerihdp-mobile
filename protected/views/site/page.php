<div class="page_detail">
    <div class="page_content">
        <?php foreach ($blocks as $block) { ?>
            <div id='<?php echo $block->url ?>' class="page_block" style="background-color: <?php echo $block->bgcolor ?>;">
                <div class="content">
                    <h3><?php echo $block->title ?></h3>
                    <?php echo $block->description ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>