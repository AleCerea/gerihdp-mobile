<div class="band s1 c-white">
    <?php
    $privacy_check_labels = array(
        "IT" => array(
            "it" => "ACCONSENTO AL TRATTAMENTO DEI DATI PERSONALI AI SENSI DEL D.LGS. 196/03",
            "en" => "I AUTHORISE THE USE OF MY PERSONAL DATA IN COMPLIANCE WITH D. LGS. 196/03",
        ),
        "CO" => array(
            "it" => "ACCONSENTO AL TRATTAMENTO DEI DATI PERSONALI AI SENSI DEL D.LGS. 196/03",
            "en" => "I AUTHORISE THE USE OF MY PERSONAL DATA IN COMPLIANCE WITH D. LGS. 196/03",
        ),
        "RO" => array(
            "ro" => "SUN DE ACORD CU PRELUCRAREA DATELOR CU CARACTER PERSONAL , ÎN CONFORMITATE CU LEGEA 677/2001",
            "en" => "I AUTHORISE THE USE OF MY PERSONAL DATA IN COMPLIANCE WITH legea 677/2001"
        ),
        "FR" => array(
            "fr" => "je consens au traitement de mes données personnelles en conformité avec: <a href='http://m.gerihdp.com/fr/gouvernance/vie-privee#mentions-legales' target='_blank'>MENTIONS LÉGALES RELATIVES AU PARTAGE D'INFORMATIONS PERSONNELLES</a>",
            "en" => "I AUTHORISE THE USE OF MY PERSONAL DATA IN COMPLIANCE WITH: <a href='http://m.gerihdp.com/fr/governance/privacy#privacy' target='_blank'>MENTIONS LÉGALES RELATIVES AU PARTAGE D'INFORMATIONS PERSONNELLES</a>"
        ),
        "DE" => array(
            "de" => "Ich gebe meine Zustimmung auf persönlichen Angaben  vor, nach ggf",
            "en" => "I authorise the use of my personal data in compliance with D. Lgs. 196/03",
        ),
        "UK" => array(
            "en" => "I authorise the use of my personal data in compliance with D. Lgs. 196/03",
        ),
        "ES" => array(
            "es" => "HE LEÍDO Y ACEPTO LAS CONDICIONES DE TRATAMIENTO DE DATOS PERSONALES",
            "en" => "I authorise the use of my personal data in compliance with D. Lgs. 196/03",
        ),
    );

    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'contact-form',
        'enableClientValidation' => false,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    ));
    ?>
    <?php if (Yii::app()->user->hasFlash("contact_sent") && Yii::app()->user->getFlash("contact_sent")) { ?>
        <div class="confirm_message"><?php echo Yii::t("site", "Richiesta inviata con successo, sarà ricontattato quanto prima.") ?></div>
    <?php } ?>
    <div class="band-inner">
        <div class="drawer">
            <div class="drawer-body">
                <strong><?php echo Text::getTextByKey("Contatti - Testo intro", true) ?></strong>
                <div class="row">
                    <?php echo $form->labelEx($model, Yii::t("site", "Nome")); ?>                	 
                    <?php echo $form->textField($model, 'nome', array('placeholder' => Yii::t("site", 'Inserisci il tuo nome'))); ?>
                    <?php echo $form->error($model, 'nome'); ?>
                </div>
                <div class="row">
                    <?php echo $form->error($model, 'cognome'); ?>
                    <?php echo $form->labelEx($model, Yii::t("site", 'cognome')); ?>
                    <?php echo $form->textField($model, 'cognome', array('placeholder' => Yii::t("site", 'Inserisci il tuo cognome'))); ?>
                </div>
                <div class="row">
                    <?php echo $form->error($model, 'email'); ?>
                    <?php echo $form->labelEx($model, Yii::t("site", "E-mail")); ?>
                    <?php echo $form->textField($model, 'email', array('placeholder' => Yii::t("site", 'Inserisci la tua email'))); ?>
                </div>
                <div class="row">
                    <?php echo $form->error($model, 'societa'); ?>
                    <?php echo $form->labelEx($model, Yii::t("site", "Società")); ?>
                    <?php echo $form->textField($model, 'societa', array('placeholder' => Yii::t("site", 'Inserisci il nome della società'))); ?>
                </div>
                <div class="row">
                    <?php echo $form->error($model, 'telefono'); ?>
                    <?php echo $form->labelEx($model, Yii::t("site", "Telefono")); ?>
                    <?php echo $form->textField($model, 'telefono', array('placeholder' => Yii::t("site", 'Inserisci il tuo numero'))); ?>
                </div>

                <?php if (CCaptcha::checkRequirements()): ?>
                    <?php echo $form->error($model, 'verifyCode'); ?>
                    <?php $this->widget('CCaptcha', array("buttonLabel" => Yii::t("site", "Genera un codice nuovo"))); ?>
                    <div class="row">
                        <label><?Php echo Yii::t("site", "Codice") ?></label>
                        <?php echo $form->textField($model, 'verifyCode'); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="drawer">
                <div class="drawer-body">
                    <?php echo $form->error($model, 'messaggio'); ?>
                    <div class="drawer-input required"><label><?php echo Yii::t("site", "Messaggio") ?></label></div>
                    <div class="drawer-input required"><?php echo $form->textArea($model, 'messaggio', array('placeholder' => Yii::t("site", 'Scrivi un messaggio'), 'style' => 'width:97%;')); ?></textarea></div>
                    <?php echo $form->error($model, 'privacy'); ?>
                    <div class="drawer-input required"><label><?php echo $form->checkBox($model, 'privacy', array('value' => 1, 'uncheckValue' => "")); ?><?php echo $privacy_check_labels[CountryManager::getCountryCode()][Yii::app()->language] ?></label></div>
                    <div class="drawer-input"><input class="right" type="submit" value="<?php echo Yii::t("site", "Invia") ?>"></div>
                </div>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>
<style type="text/css">
    #yw0_button{
        display: none;
    }
</style>