<div id = "footer">
    <?php echo Text::getTextByKey("Footer"); ?>
    Credits: <a href = "http://www.ipomea.it" target="_blank">ipomea.it</a>
</div><!--footer -->

<?php $this->renderPartial("//layouts/_cookie_bar") ?>

<!-- Pure Chat Snippet -->
<script type='text/javascript'>
    (function () {
        var done = false;
        var script = document.createElement('script');
        script.async = true;
        script.type = 'text/javascript';
        script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';
        document.getElementsByTagName('HEAD').item(0).appendChild(script);
        script.onreadystatechange = script.onload = function (e) {
            if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                var w = new PCWidget({c: 'a4aa32b0-8487-4963-9ae6-459ee1057356', f: true});
                done = true;
            }
        };
    })();
</script>
<!-- End Pure Chat Snippet -->
