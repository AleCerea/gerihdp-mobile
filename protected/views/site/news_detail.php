<?php
$this->pageTitle = $menuItem->label;
?>
<div class="page_detail">
    <div class="content">
        <h1><?php echo $model->titolo ?></h1>
        <div class="page_content news_detail">

            <?php if ($model->url_immagine != NULL) { ?>
                <a href="<?php echo $model->url_immagine ?>" target="_blank"><img class="news_image" src="<?php echo $model->getImage() ?>" alt="" /></a>
            <?php } else { ?>
                <img class="news_image" src="<?php echo $model->getImage() ?>" alt="" />
            <?php } ?>

            <div class='clear'></div>

            <small><?php echo date("d/m/Y", strtotime($model->data)) ?> - <?php echo $model->getTipologia() ?></small>
            <?php if ($model->categoria != NULL && $model->categoria != "" && $model->categoria != 0) { ?>
                <h5><?php echo $model->getCategory() ?></h5>
            <?php } ?>
            <div class='clear'></div>


            <div class="news_content"><?php echo $model->testo ?>

                <?php if ($model->tipo == 1 && $model->allegato != null && $model->allegato != "") { ?>
                    <div class="attachments">
                        <a href="<?php echo Yii::app()->baseUrl . "/uploads/attachments/" . $model->allegato; ?>" target="_blank">Allegato</a>
                    </div>
                <?php } ?>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>