<script type="text/javascript">
    $(function () {

        $('#mainmenu li').mouseover(function (e) {
            var centerLeft = $(this).position().left + $(this).width() / 2;

            var submenu = $(this).children(".submenu");
            if ($('#mainmenu').offset().left + centerLeft + submenu.width() / 2 > $(window).width()) {
                $(this).children(".submenu").css("right", "0px");
            } else {
                $(this).children(".submenu").css("left", (centerLeft - submenu.width() / 2) + "px");
            }
        });
    });

</script>
<div id="pre_header">
    <div class="content-fluid">

        <div class="float_right">
            <div id="logos" class="float_left" />
            <div id="social_menu" class="float_left">
                <?php
                $inUrl = Config::getConfigByKey("inurl");
                $twUrl = Config::getConfigByKey("twurl");
                $fbUrl = Config::getConfigByKey("fburl");

                if ($inUrl != null && $inUrl != "") {
                    ?>
                    <a href="<?php echo $inUrl ?>" target="_blank"><img src="<?php echo Yii::app()->baseUrl ?>/images/commons/icon_linkedin.png" alt="Linkedin" /></a>
                <?php } ?>
                <?php if ($twUrl != null && $twUrl != "") { ?>
                    <a href="<?php echo $twUrl ?>" target="_blank"><img src="<?php echo Yii::app()->baseUrl ?>/images/commons/icon_twitter.png" alt="Twitter" /></a>
                <?php } ?>
                <?php if ($fbUrl != null && $fbUrl != "") { ?>
                    <a href = "<?php echo $fbUrl ?>" target = "_blank"><img src = "<?php echo Yii::app()->baseUrl ?>/images/commons/icon_facebook.png" alt = "Facebook" /></a>
                <?php } ?>
            </div>
            <div id="header_phone" class="float_left">
                <img src="<?php echo Yii::app()->baseUrl ?>/images/commons/icon_phone.png" alt="Telefono" />
                <span><?php echo Config::getConfigByKey("telefono") ?></span><br/>
            </div>

            <!--            <div id="live_chat_button" class="float_left">
                            <img src="<?php // echo Yii::app()->baseUrl               ?>/images/commons/live_chat_ico.png" alt="Chat" />
                            <span>LIVE CHAT</span>
                        </div>-->

            <div id="nazione_select" class="float_left">
                <div id="nazione_button" class="float_left">
                    <?php echo CountryManager::getCountry()->name ?>
                </div>
                <img src="<?php echo Yii::app()->baseUrl ?>/images/commons/arrow_down.png" />

                <div id="nazione_select_list">
                    <?php
                    foreach (CountryManager::getCountries() as $country) {
                        if ($country->code == "DE")
                            continue;
                        ?>
                        <a href="<?php echo $this->createUrl('home/changeCountry', array('country' => $country->code)) ?>">
                            <div class="country_item">
                                <?php echo $country->name ?>
                            </div>
                        </a>
                    <?php } ?>
                </div>
            </div>

            <?php
            $languages = CountryManager::getLanguages();

            if (count($languages) > 1) {
                ?>
                <div id="languages" class="float_left">
                    <?php foreach ($languages as $lang) { ?>
                        <a href="<?php echo $this->createUrl('', array('lang' => $lang)) ?>">
                            <div class="lang float_left <?php echo (Yii::app()->language == $lang) ? "active" : "" ?>">
                                <?php echo strtoupper($lang) ?>
                            </div>
                        </a>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div id="header_container" class="content">
    <div id="logo" class="float_left">
        <?php
        $countryCode = CountryManager::getCountryCode();
        $cByDomain = CountryManager::getCountryByDomain();
        ?>
        <a href="<?php echo Yii::app()->getBaseUrl(true) ?>">
            <?php if ($cByDomain === NULL) { ?>
                <img src="<?php echo Yii::app()->baseUrl ?>/images/commons/logo_gerihdp.png" alt="GeriHDP" />
            <?php } else { ?>
                <img src="<?php echo Yii::app()->baseUrl ?>/images/commons/logo.png" alt="GeriHDP" />
            <?php } ?>
        </a>
    </div>


    <div id="mainmenu" class="float_right">
        <ul>
            <li><a href="<?php echo Yii::app()->homeUrl ?>">HOME</a></li>
            <?php
            foreach (Menu::getItems() as $menuItem) {
                if ($menuItem->static_type == 'news' && $countryCode != 'UK' && Yii::app()->language == 'en') {
                    continue;
                }
                
                $pages = $menuItem->getPages();
                if (count($pages) == 1 && $menuItem->static == 1) {
                    ?>
                    <a href="<?php echo $this->createUrl('/home/page', array('country' => strtolower(CountryManager::getCountryCode()), 'menu' => $menuItem->url)) ?>">
                        <li><?php echo strtoupper($menuItem->label) ?></li>
                    </a>
                    <?php
                } else {
                    $countPages = count($menuItem->getPages());

                    if ($countPages == 0) {
                        echo '<a href="' . $this->createUrl('home/page', array('country' => strtolower(CountryManager::getCountryCode()), 'menu' => $menuItem->url, 'page' => '')) . '">';
                    }
                    ?>
                    <li><?php echo strtoupper($menuItem->label) ?>
                        <?php if ($countPages > 0) { ?>
                            <div class="submenu" style="width:<?php echo ((count($pages) <= 3) ? (count($pages) * 270) : 3 * 270 ) ?>px;">
                                <div class="submenu_container">
                                    <?php foreach ($menuItem->getPages() as $num => $page) { ?>
                                        <?php if ($num > 0 && ($num % 3) == 0) { ?>
                                            <div class="clear"></div>
                                        <?php } ?>
                                        <div class="submenu_item <?php echo (count($page->getBlocks()) == 0) ? 'single-item' : '' ?>">
                                            <a href="<?php echo $this->createUrl('/home/page', array('country' => strtolower(CountryManager::getCountryCode()), 'menu' => $menuItem->url, 'page' => $page->url)) ?>"><h3><?php echo $page->title ?></h3></a>
                                            <?php foreach ($page->getBlocks() as $block) { ?>
                                                <a href="<?php echo $this->createUrl('/home/page', array('country' => strtolower(CountryManager::getCountryCode()), 'menu' => $menuItem->url, 'page' => $page->url)) ?>#<?php echo $block->url ?>"><div class="submenu_link"><?php echo $block->title ?></div></a>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        <?php } ?>
                    </li>
                    <?php
                    if ($countPages == 0) {
                        echo '</a>';
                    }
                }
            }
            ?>
        </ul>
    </div>
    <div class="clear"></div>
</div>