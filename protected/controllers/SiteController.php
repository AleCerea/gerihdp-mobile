<?php

class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    protected function beforeAction($action) {
        $countryCode = CountryManager::getCountryCode();
        if (!in_array(strtolower($countryCode), CountryManager::getCountryAvailables())) {
            CountryManager::setCountry("IT");
        }

        // set country immediately for enabled localized domain
        $cByDomain = CountryManager::getCountryByDomain();
        if ($cByDomain !== NULL) {
            CountryManager::setCountry(strtoupper($cByDomain));
        }
        else {
            CountryManager::setCountry("CO");
        }

        $app = Yii::app();
        if (isset($app->session['site_lang'])) {
            $app->language = $app->session['site_lang'];
        }
        
        $languages = CountryManager::getLanguages();
        if (!in_array($app->language, $languages)) {
            $app->session['site_lang'] = $languages[0];
            Yii::app()->language = $languages[0];
        }

        if ($action->id == "page") {
            $menu = $_GET['menu'];
            $selectedMenu = Menu::model()->localized()->findByLocalizedAttributes(array('url' => $menu, 'country' => CountryManager::getCountryCode()));

            Yii::app()->session['menu'] = $selectedMenu;
            Yii::app()->session['page'] = null;

            if ($selectedMenu != null && ($selectedMenu->static != 1 || count($selectedMenu->getPages()) > 0)) {
                if ($selectedMenu->visible == 1) {
                    $page = isset($_GET['page']) ? $_GET['page'] : null;

                    if ($selectedMenu->static == 1) {
                        $pages = $selectedMenu->getPages();
                        $selectedPage = $pages[0];
                    } else {
                        $selectedPage = Page::model()->localized()->findByLocalizedAttributes(array('url' => $page, 'parent_menu_id' => $selectedMenu->id, 'visible' => 1, 'country' => CountryManager::getCountryCode()));
                    }
                    Yii::app()->session['page'] = $selectedPage;
                }
            }
        }

        if (isset($_GET['lang'])) {
            $languages = CountryManager::getLanguages();
            $lang = $_GET['lang'];

            if (in_array($lang, $languages)) {
                $app->language = $lang;
                $app->session['site_lang'] = $app->language;
            }

            $selectedMenu = Yii::app()->session['menu'];
            $selectedPage = Yii::app()->session['page'];

            if ($selectedMenu !== null) {
                $menuLang = Menu::model()->localized()->findByPk($selectedMenu->id);
                if ($selectedPage !== null) {
                    $pageLang = Page::model()->localized()->findByPk($selectedPage->id);

                    $this->redirect($this->createUrl('page', array('country' => strtolower(CountryManager::getCountryCode()), 'menu' => $menuLang->url, 'page' => $pageLang->url)));
                } else {
                    $this->redirect($this->createUrl('page', array('country' => strtolower(CountryManager::getCountryCode()), 'menu' => $menuLang->url)));
                }
            } else {
                $this->redirect($this->createUrl('index'));
            }
        } else if (isset($app->session['site_lang'])) {
            $app->language = $app->session['site_lang'];
        }

        return true;
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        if (isset($_GET['country']) && CountryManager::getCountryCode() != strtoupper($_GET['country']) && $_GET['country'] != "" && in_array(strtolower($_GET['country']), CountryManager::getCountryAvailables())) {
            CountryManager::setCountry(strtoupper($_GET['country']));

            $country = CountryManager::getCountry();

            $langs = CountryManager::getLanguages();
            if (!in_array(Yii::app()->language, $langs)) {
                Yii::app()->language = $country->primary_lang;
                Yii::app()->session['site_lang'] = Yii::app()->language;
            }

            $cByDomain = CountryManager::getCountryByDomain();
            if ($cByDomain !== NULL) {
                $this->redirect(array('index'));
            }
        }

        unset(Yii::app()->session['menu']);
        unset(Yii::app()->session['page']);

        $homeData = Home::model()->localized()->findByAttributes(array('country' => CountryManager::getCountryCode()));

        if ($homeData !== null) {
            $this->pageTitle = $homeData->title;
            Yii::app()->clientScript->registerMetaTag($homeData->description, 'description');
            Yii::app()->clientScript->registerMetaTag($homeData->keywords, 'keywords');
        } else {
            $this->pageTitle = Yii::t("site", "GERI HDP, la Gestione del tuo Credito");
            Yii::app()->clientScript->registerMetaTag(Yii::t("site", "Ci occupiamo di Gestione e Recupero del Credito da oltre vent’anni, in Italia e all’Estero. Visita il sito e scopri tutti i nostri Servizi."), 'description');
            Yii::app()->clientScript->registerMetaTag(Yii::t("site", "GERI HDP, Credito, Recupero Crediti, insoluti, gestione del credito"), 'keywords');
        }

        $box1 = Textbox::model()->localized()->findAllByAttributes(array('box' => "box1", 'nazione' => CountryManager::getCountryCode(), 'visibile' => 1), array('order' => 'id DESC'));
        $box2 = Textbox::model()->localized()->findAllByAttributes(array('box' => "box2", 'nazione' => CountryManager::getCountryCode(), 'visibile' => 1), array('order' => 'id DESC'));
        $box3 = Textbox::model()->localized()->findAllByAttributes(array('box' => "box3", 'nazione' => CountryManager::getCountryCode(), 'visibile' => 1), array('order' => 'id DESC'));
        $boxS = Textbox::model()->localized()->findAllByAttributes(array('box' => "boxS", 'nazione' => CountryManager::getCountryCode(), 'visibile' => 1), array('order' => 'id DESC'));

        $this->render("index", array('box1' => $box1, 'box2' => $box2, 'box3' => $box3, 'boxS' => $boxS));
    }

    public function actionChangeCountry($country) {
        CountryManager::setCountry($country);

        $country = CountryManager::getCountry();

        Yii::app()->language = $country->primary_lang;
        Yii::app()->session['site_lang'] = Yii::app()->language;

        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionPage($menu, $page = null) {

        $selectedMenu = Menu::model()->localized()->findByLocalizedAttributes(array('url' => $menu,  'country' => CountryManager::getCountryCode()));

        if ($selectedMenu == NULL) {
            // find in english
            if (Yii::app()->language == 'en') {
                $country = CountryManager::getCountry();
                Yii::app()->language = $country->primary_lang;
            } else {
                Yii::app()->language = 'en';
            }
           // Yii::app()->session['site_lang'] = Yii::app()->language;
            $selectedMenu = Menu::model()->localized()->findByLocalizedAttributes(array('url' => $menu, 'country' => CountryManager::getCountryCode()));
        }

        Yii::app()->session['menu'] = $selectedMenu;
        Yii::app()->session['page'] = null;
        

        if ($selectedMenu === null)
            throw new CHttpException(404);

        if ($selectedMenu->static == 1 && count($selectedMenu->getPages()) == 0) {
            $action = "render" . ucfirst($selectedMenu->static_type);
            $this->$action($selectedMenu, $page);
        } else {
            if ($selectedMenu->visible == 0)
                throw new CHttpException(404);

            if ($selectedMenu->static == 1) {
                $pages = $selectedMenu->getPages();
                $selectedPage = $pages[0];
            } else {
                $selectedPage = Page::model()->localized()->findByLocalizedAttributes(array('url' => $page, 'parent_menu_id' => $selectedMenu->id, 'visible' => 1, 'country' => CountryManager::getCountryCode()));
            }
            
            if ($selectedPage == NULL) {
                $selectedPage = Page::model()->localized()->findByLocalizedAttributes(array('url' => $page, 'parent_menu_id' => $selectedMenu->id, 'country' => CountryManager::getCountryCode()));

                if ($selectedPage !== NULL && $selectedPage->static == 1 && $selectedPage->static_type == 'news_governance') {
                    $newsItem = Menu::getVoceByStaticType("news");
                    $this->redirect($this->createUrl('home/page', array('country' => strtolower(CountryManager::getCountryCode()), 'menu' => $newsItem->url, 'page' => '')));
                }
            }
            
            if ($selectedPage === null)
                throw new CHttpException(404);

            $this->pageTitle = $selectedPage->title;

            Yii::app()->clientScript->registerMetaTag($selectedPage->description, 'description');
            Yii::app()->clientScript->registerMetaTag($selectedPage->keywords, 'keywords');

            Yii::app()->session['page'] = $selectedPage;

            if ($selectedPage->static == 1) {
                $action = "render" . ucfirst($selectedPage->static_type);
                $this->$action($selectedMenu, $selectedPage);
            } else {
                $blocks = $selectedPage->getBlocks();
                $this->render("page", array('model' => $selectedPage, 'blocks' => $blocks));
            }
        }
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    private function renderDovesiamo($selectedMenu, $page) {
        $sedi = Sede::getSedi();

        $this->render("static_page", array('menuItem' => $selectedMenu, 'path' => "//site/_dove_siamo", 'params' => array("menuItem" => $selectedMenu, 'sedi' => $sedi)));
    }

    private function renderNews_governance($selectedMenu, $pageValue = null) {
        // find news and redirect
        $menu = Menu::model()->localized()->findByAttributes(array("static_type" => "news"));

        $this->renderNews($menu);
    }

    private function renderNews($selectedMenu, $pageValue = null, $slug = null) {
        $this->pageTitle = strip_tags($selectedMenu->label);

        if ($pageValue != null) {
            if (is_numeric($pageValue)) {
                $model = News::model()->localized()->findByPk($pageValue);
            } else {
                $model = News::model()->localized()->findByLocalizedAttributes(array('url' => $pageValue));
            }
            
            Yii::app()->clientScript->registerMetaTag($model->titolo, null, null, array('property' => 'og:title'));

            $description = strip_tags($model->testo_intro);

            Yii::app()->clientScript->registerMetaTag($description, 'description');
            Yii::app()->clientScript->registerMetaTag($description, null, null, array('property' => 'og:description'));
            Yii::app()->clientScript->registerMetaTag("article", null, null, array('property' => 'og:type'));
            Yii::app()->clientScript->registerMetaTag(Yii::app()->request->hostInfo . Yii::app()->request->url, null, null, array('property' => 'og:url'));

            $this->render("news_detail", array("menuItem" => $selectedMenu, "model" => $model));
        } else {
            Yii::app()->clientScript->registerMetaTag($this->pageTitle, null, null, array('property' => 'og:title'));

            $model = new News;

            Yii::app()->clientScript->registerMetaTag(Yii::t("site", "Ci occupiamo di Gestione e Recupero del Credito da oltre vent’anni, in Italia e all’Estero. Visita il sito e scopri tutti i nostri Servizi."), 'description');
            Yii::app()->clientScript->registerMetaTag(Yii::t("site", "Ci occupiamo di Gestione e Recupero del Credito da oltre vent’anni, in Italia e all’Estero. Visita il sito e scopri tutti i nostri Servizi."), null, null, array('property' => 'og:description'));
            Yii::app()->clientScript->registerMetaTag(Yii::app()->getBaseUrl(true) . "/images/logo-gerihdp.jpg", null, null, array('property' => 'og:image'));

            $this->render("static_page", array('menuItem' => $selectedMenu, 'path' => "//site/_news", 'params' => array("model" => $model, "menuItem" => $selectedMenu)));
        }
    }

    private function renderContatti_new($selectedMenu, $page) {
        $model = new ContactForm;

        $sedi = Sede::getSedi();
        
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $mail = Yii::app()->params['contact_email'];
                if (CountryManager::getCountryCode() == "RO") {
                    $mail = "info@geri.ro";
                } else if (CountryManager::getCountryCode() == "FR") {
                    $mail = "information@geri.fr";
                }

                Mailer::sendMail($mail, "Richiesta contatto", "contact_request", array('model' => $model), $model->email);

                $contact = new Contact;
                $contact->nome = $model->nome;
                $contact->cognome = $model->cognome;
                $contact->email = $model->email;
                $contact->telefono = $model->telefono;
                $contact->societa = $model->societa;
                $contact->messaggio = $model->messaggio;
                $contact->data_inserimento = date("Y-m-d H:i:s");
                $contact->nazione = CountryManager::getCountryCode();
                $contact->lingua = Yii::app()->language;
                $contact->save();


                Yii::app()->user->setFlash("contact_sent", true);
                $this->redirect(Yii::app()->request->url);
            }
        }

        $this->render("static_page", array('menuItem' => $selectedMenu, 'path' => "//site/_contatti_new", 'params' => array("model" => $model, "menuItem" => $selectedMenu, 'sedi' => $sedi)));
    }

    public function actionSetCookies() {
        setcookie("cookie_bar", true, strtotime('+365 days'), "/");

        die('');
    }

}
