<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$box1 = Textbox::model()->localized()->findAllByAttributes(array('box' => 1, 'nazione' => CountryManager::getCountryCode(), 'visibile' => 1), array('order' => 'id DESC'));
        $box2 = Textbox::model()->localized()->findAllByAttributes(array('box' => 2, 'nazione' => CountryManager::getCountryCode(), 'visibile' => 1), array('order' => 'id DESC'));
        $box3 = Textbox::model()->localized()->findAllByAttributes(array('box' => 3, 'nazione' => CountryManager::getCountryCode(), 'visibile' => 1), array('order' => 'id DESC'));
		
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render("index", array('box1' => $box1, 'box2' => $box2, 'box3' => $box3));
	}
	
	
	public function actionChangeCountry($country) {
        CountryManager::setCountry($country);

        $country = CountryManager::getCountry();

        Yii::app()->language = $country->primary_lang;
        Yii::app()->session['site_lang'] = Yii::app()->language;

        $this->redirect(Yii::app()->baseUrl);
    }

    public function actionPage($menu, $page = null) {
        	
       $selectedMenu = Menu::model()->localized()->findByLocalizedAttributes(array('url' => $menu));

        if ($selectedMenu === null)
            throw new CHttpException(404);

        if ($selectedMenu->static == 1 && count($selectedMenu->getPages()) == 0) {
            $action = "render" . ucfirst($selectedMenu->static_type);
            $this->$action($selectedMenu, $page);
        } else {
            if ($selectedMenu->visible == 0)
                throw new CHttpException(404);

            if ($selectedMenu->static == 1) {
                $pages = $selectedMenu->getPages();
                $selectedPage = $pages[0];
            } else {
                $selectedPage = Page::model()->localized()->findByLocalizedAttributes(array('url' => $page, 'parent_menu_id' => $selectedMenu->id, 'visible' => 1));
            }

            if ($selectedPage === null)
                throw new CHttpException(404);

            $this->pageTitle = $selectedPage->title;

            Yii::app()->clientScript->registerMetaTag($selectedPage->description, 'description');
            Yii::app()->clientScript->registerMetaTag($selectedPage->keywords, 'keywords');


            if ($selectedPage->static == 1) {
                $action = "render" . ucfirst($selectedPage->static_type);
                $this->$action($selectedMenu, $selectedPage);
            } else {
                $blocks = $selectedPage->getBlocks();
                $this->render("page", array('model' => $selectedPage, 'blocks' => $blocks));
            }
        }
    }
	

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

    private function renderDovesiamo($selectedMenu, $page) {
        $sedi = Sede::getSedi();

        $this->render("static_page", array('menuItem' => $selectedMenu, 'path' => "//site/_dove_siamo", 'params' => array("menuItem" => $selectedMenu, 'sedi' => $sedi)));
    }
	
	private function renderNews_governance($selectedMenu, $pageValue = null) {
        // find news and redirect
        $menu = Menu::model()->localized()->findByAttributes(array("static_type" => "news"));

        $this->renderNews($menu);
    }

    private function renderNews($selectedMenu, $pageValue = null) {
        $this->pageTitle = $selectedMenu->label;

        if ($pageValue != null) {
            $model = News::model()->localized()->findByPk($pageValue);

            $this->render("news_detail", array("menuItem" => $selectedMenu, "model" => $model));
        } else {
            $model = new News;

            $this->render("static_page", array('menuItem' => $selectedMenu, 'path' => "//site/_news", 'params' => array("model" => $model, "menuItem" => $selectedMenu)));
        }
    }

    private function renderContatti($selectedMenu, $page) {
        $model = new ContactForm;

        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                Mailer::sendMail(Yii::app()->params['contact_email'], "Richiesta contatto", "contact_request", array('model' => $model), $model->email);

                $contact = new Contact;
                $contact->nome = $model->nome;
                $contact->cognome = $model->cognome;
                $contact->email = $model->email;
                $contact->telefono = $model->telefono;
                $contact->societa = $model->societa;
                $contact->messaggio = $model->messaggio;
                $contact->data_inserimento = date("Y-m-d H:i:s");
                $contact->nazione = CountryManager::getCountryCode();
                $contact->lingua = Yii::app()->language;
                $contact->save();


                Yii::app()->user->setFlash("contact_sent", true);
                $this->redirect(Yii::app()->request->url);
            }
        }

        $this->render("static_page", array('menuItem' => $selectedMenu, 'path' => "//site/_contatti", 'params' => array("model" => $model, "menuItem" => $selectedMenu)));
    }
}