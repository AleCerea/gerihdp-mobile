<?php

/**
 * This is the model class for table "sliderLang".
 *
 * The followings are the available columns in table 'sliderLang':
 * @property integer $sliderId
 * @property string $lang
 * @property string $titolo
 * @property string $label1
 * @property string $label2
 * @property string $label3
 * @property string $label4
 * @property string $label5
 * @property string $label6
 */
class SliderLang extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'sliderLang';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('sliderId, lang, titolo', 'required'),
            array('sliderId', 'numerical', 'integerOnly' => true),
            array('lang', 'length', 'max' => 2),
            array('titolo', 'length', 'max' => 128),
            array('label1, label2, label3, label4, label5, label6', 'length', 'max' => 100),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('sliderId, lang, titolo, label1, label2, label3, label4, label5, label6', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'sliderId' => 'Slider',
            'lang' => 'Lang',
            'titolo' => 'Titolo',
            'label1' => 'Label1',
            'label2' => 'Label2',
            'label3' => 'Label3',
            'label4' => 'Label4',
            'label5' => 'Label5',
            'label6' => 'Label6',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('sliderId', $this->sliderId);
        $criteria->compare('lang', $this->lang, true);
        $criteria->compare('titolo', $this->titolo, true);
        $criteria->compare('label1', $this->label1, true);
        $criteria->compare('label2', $this->label2, true);
        $criteria->compare('label3', $this->label3, true);
        $criteria->compare('label4', $this->label4, true);
        $criteria->compare('label5', $this->label5, true);
        $criteria->compare('label6', $this->label6, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SliderLang the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
