<?php

/**
 * This is the model class for table "news".
 *
 * The followings are the available columns in table 'news':
 * @property integer $id
 * @property integer $tipo
 * @property string $immagine
 * @property string $url_immagine
 * @property string $titolo
 * @property string $testo_intro
 * @property string $testo
 * @property string $data
 * @property string $allegato
 * @property string $luogo
 * @property string $country
 * @property integer $visible
 * @property integer $categoria
 *
 * The followings are the available model relations:
 * @property NewsLang[] $newsLangs
 */
class News extends MultilingualActiveRecord {

    public static $THUMB_WIDTH = 300;
    public static $THUMB_HEIGHT = 120;

    public function init() {
        $this->country = CountryManager::getCountryCode();

        parent::init();
    }

    //primary language
    public function primaryLang() {
        $country = Country::model()->findByPk(($this->country) ? $this->country : CountryManager::getCountryCode());

        return $country->primary_lang;
    }

    //additional languages found in the translations table
    public function languages() {
        if ($this->primaryLang() != "en") {
            return array("en");
        } else
            return array();
    }

    //attributes to look for in the translations table
    public function localizedAttributes() {
        return array('titolo', 'testo_intro', 'testo', 'allegato');
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'news';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        $rules = array(
            array('tipo, titolo, testo_intro, testo, data, country, categoria', 'required'),
            array('tipo, visible', 'numerical', 'integerOnly' => true),
            array('immagine, allegato', 'length', 'max' => 256),
            array('titolo', 'length', 'max' => 120),
            array('luogo', 'length', 'max' => 500),
            array('country', 'length', 'max' => 2),
            array('immagine', 'file',
                'types' => 'jpg, jpeg, gif, png',
                'allowEmpty' => false,
                'on' => 'insert',
                'safe' => false
            ),
            array('immagine', 'file',
                'types' => 'jpg, jpeg, gif, png',
                'allowEmpty' => true,
                'on' => 'update',
                'safe' => false
            ),
            array('allegato', 'file',
                'types' => 'pdf',
                'allowEmpty' => true,
                'safe' => false
            ),
            array('immagine, url_immagine', 'safe'),
            array('id, tipo, immagine, titolo, testo_intro, testo, data, allegato, luogo, country, visible', 'safe', 'on' => 'search'),
        );


        foreach ($this->languages() as $lang) {
            $rules[] = array("titolo_$lang, testo_intro_$lang, testo_$lang", 'required');
            $rules[] = array("titolo_$lang, testo_intro_$lang, testo_$lang", "safe");

            $rules[] = array("allegato_$lang", 'length', 'max' => 256);
            $rules[] = array("allegato_$lang", 'file',
                'types' => 'pdf',
                'allowEmpty' => true
            );
            $rules[] = array("allegato_$lang", 'safe');
        }


        return $rules;
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'newsLangs' => array(self::HAS_MANY, 'NewsLang', 'newsId'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        $labels = array(
            'id' => 'ID',
            'tipo' => Yii::t('admin', 'Tipo'),
            'immagine' => Yii::t('admin', 'Immagine'),
            'titolo' => Yii::t('admin', 'Titolo') . ' (' . strtoupper($this->primaryLang()) . ')',
            'testo_intro' => Yii::t('admin', 'Testo Introduttivo') . ' (' . strtoupper($this->primaryLang()) . ')',
            'testo' => Yii::t('admin', 'Testo') . ' (' . strtoupper($this->primaryLang()) . ')',
            'data' => Yii::t('admin', 'Data'),
            'allegato' => Yii::t('admin', 'Allegato') . ' (' . strtoupper($this->primaryLang()) . ')',
            'luogo' => Yii::t('admin', 'Luogo'),
            'country' => 'Country',
            'visible' => Yii::t('admin', 'Visibile'),
            'url_immagine' => 'URL immagine'
        );


        foreach ($this->languages() as $lang) {
            $labels["titolo_$lang"] = Yii::t('admin', 'Titolo') . ' (' . strtoupper($lang) . ')';
            $labels["testo_intro_$lang"] = Yii::t('admin', 'Testo Introduttivo') . ' (' . strtoupper($lang) . ')';
            $labels["testo_$lang"] = Yii::t('admin', 'Testo') . ' (' . strtoupper($lang) . ')';
            $labels["allegato_$lang"] = Yii::t('admin', 'Allegato') . ' (' . strtoupper($lang) . ')';
        }

        return $labels;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('tipo', $this->tipo);
        $criteria->compare('immagine', $this->immagine, true);
        $criteria->compare('titolo', $this->titolo, true);
        $criteria->compare('testo', $this->testo, true);
        $criteria->compare('data', $this->data, true);
        $criteria->compare('allegato', $this->allegato, true);
        $criteria->compare('luogo', $this->luogo, true);
        $criteria->compare('country', $this->country, true);
        $criteria->compare('visible', $this->visible);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchAdmin() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('tipo', $this->tipo);
        $criteria->compare('immagine', $this->immagine, true);
        $criteria->compare('titolo', $this->titolo, true);
        $criteria->compare('testo', $this->testo, true);
        $criteria->compare('data', $this->data, true);
        $criteria->compare('allegato', $this->allegato, true);
        $criteria->compare('luogo', $this->luogo, true);
        $criteria->compare('country', CountryManager::getCountryCode(), true);
        $criteria->compare('visible', $this->visible);
        $criteria->compare('categoria', $this->categoria);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'id DESC'
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return News the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getNazione() {
        $country = Country::model()->findByPk($this->country);

        return $country->name;
    }

    public function getTipologia() {
        $tipos = self::getTipologie();
        return $tipos[$this->tipo];
    }

    public static function getTipologie() {
        return array(
            4 => Yii::t('site', "Approfondimenti"),
            2 => Yii::t('site', "Evento"),
            3 => Yii::t('site', "Notizie"),
            0 => Yii::t('site', "Rassegna Stampa")
        );
    }

    public static function getImagesPath() {
        return Yii::app()->basePath . "/../uploads/images/";
    }

    public function getImage() {
        return "http://www.geri.it/uploads/images/" . $this->immagine;
    }

    public static function getImagesThumbPath() {
        return Yii::app()->basePath . "/../uploads/images/thumbs/";
    }

    public function getImageThumb() {
        return "http://www.geri.it/uploads/images/thumbs/" . $this->immagine;
    }

    public static function getAttachmentsPath() {
        return Yii::app()->basePath . "/../uploads/attachments/";
    }

    public function getAllegato($label) {
        return "http://www.geri.it/uploads/attachments/" . $this->$label;
    }

    public function searchForList() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('tipo', $this->tipo);
        $criteria->compare('immagine', $this->immagine, true);
        $criteria->compare('titolo', $this->titolo, true);
        $criteria->compare('testo', $this->testo, true);
        $criteria->compare('data', $this->data, true);
        $criteria->compare('allegato', $this->allegato, true);
        $criteria->compare('luogo', $this->luogo, true);
        $criteria->compare('country', CountryManager::getCountryCode());
        $criteria->compare('visible', 1);

        $categoria = Yii::app()->request->getQuery('categoria');
        $criteria->compare('categoria', $categoria);

        $criteria->order = "data DESC";

        return new MultilingualActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getInitialText() {
        return $this->testo_intro;
    }

    public function getCategory() {
        if ($this->categoria != NULL) {
            $categoria = CategoriaNews::model()->localized()->findByPk($this->categoria);
            if ($categoria != NULL) {
                return $categoria->nome;
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

    public function tipoFilter() {
        return TbHtml::activeDropDownList($this, 'tipo', self::getTipologie(), array('empty' => ''));
    }

    public function visibleFilter() {
        return TbHtml::activeDropDownList($this, 'visible', array('1' => 'Si', '0' => 'No'), array('empty' => ''));
    }

    public function categoryFilter() {
        return TbHtml::activeDropDownList($this, 'categoria', CHtml::listData(CategoriaNews::model()->findAllByAttributes(array('country' => CountryManager::getCountryCode())), 'id', 'nome'), array('empty' => ''));
    }

}
