<?php

/**
 * This is the model class for table "newsLang".
 *
 * The followings are the available columns in table 'newsLang':
 * @property integer $newsId
 * @property string $lang
 * @property string $titolo
 * @property string $testo_intro
 * @property string $testo
 * @property string $allegato
 *
 * The followings are the available model relations:
 * @property News $news
 */
class NewsLang extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'newsLang';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('newsId, lang, titolo, testo_intro, testo', 'required'),
            array('newsId', 'numerical', 'integerOnly' => true),
            array('lang', 'length', 'max' => 2),
            array('titolo', 'length', 'max' => 120),
            array('allegato', 'length', 'max' => 256),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('newsId, lang, titolo, testo, allegato', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'news' => array(self::BELONGS_TO, 'News', 'newsId'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'newsId' => 'News',
            'lang' => 'Lang',
            'titolo' => 'Titolo',
            'testo_intro' => 'Testo Introduttivo',
            'testo' => 'Testo',
            'allegato' => 'Allegato',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('newsId', $this->newsId);
        $criteria->compare('lang', $this->lang, true);
        $criteria->compare('titolo', $this->titolo, true);
        $criteria->compare('testo', $this->testo, true);
        $criteria->compare('allegato', $this->allegato, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return NewsLang the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
