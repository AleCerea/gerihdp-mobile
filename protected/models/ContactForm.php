<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactForm extends CFormModel {

    public $nome;
    public $cognome;
    public $email;
    public $societa;
    public $telefono;
    public $verifyCode;
    public $messaggio;
    public $privacy;

    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            // name, email, subject and body are required
            array('nome, cognome, email, messaggio', 'required'),
            array('privacy', 'required', 'message' => Yii::t("site", 'Obbligatorio')),
            // email has to be a valid email address
            array('email', 'email'),
            // verifyCode needs to be entered correctly
            array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements()),
            array('nome, cognome, email, messaggio, societa, telefono, privacy', 'safe')
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels() {
        return array(
            'nome' => Yii::t('admin', 'nome'),
            'cognome' => Yii::t('admin', 'cognome'),
            'societa' => Yii::t('admin', 'società'),
            'email' => Yii::t('admin', 'e-mail'),
            'telefono' => Yii::t('admin', 'telefono'),
            'messaggio' => Yii::t('admin', 'messaggio'),
            'verifyCode' => Yii::t('admin', 'codice'),
        );
    }

}
