<?php

/**
 * This is the model class for table "home".
 *
 * The followings are the available columns in table 'home':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $country
 */
class Home extends MultilingualActiveRecord {

    public function init() {
        $this->country = CountryManager::getCountryCode();

        parent::init();
    }

//primary language
    public function primaryLang() {
        $country = Country::model()->findByPk(($this->country) ? $this->country : CountryManager::getCountryCode());

        return $country->primary_lang;
    }

//additional languages found in the translations table
    public function languages() {
        if ($this->primaryLang() != "en") {
            return array("en");
        } else
            return array();
    }

//attributes to look for in the translations table
    public function localizedAttributes() {
        return array('title', 'description', 'keywords');
    }

    /**
     * Returns the static model of the specified AR class.
     * @return Home the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'home';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        $rules = array(
            array('title, description, keywords, country', 'required'),
            array('title, description, keywords', 'length', 'max' => 250),
            array('country', 'length', 'max' => 2),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title, description, keywords, country', 'safe', 'on' => 'search'),
        );


        foreach ($this->languages() as $l) {
            $rules[] = array('title_' . $l, 'required');
            $rules[] = array('title_' . $l, 'length', 'max' => 250);
            $rules[] = array('title_' . $l, 'safe');

            $rules[] = array('description_' . $l, 'required');
            $rules[] = array('description_' . $l, 'length', 'max' => 250);
            $rules[] = array('description_' . $l, 'safe');

            $rules[] = array('keywords_' . $l, 'required');
            $rules[] = array('keywords_' . $l, 'length', 'max' => 250);
            $rules[] = array('keywords_' . $l, 'safe');
        }

        return $rules;
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        $labels = array(
            'id' => 'ID',
            'title' => 'Title' . ' (' . strtoupper($this->primaryLang()) . ')',
            'description' => 'Description' . ' (' . strtoupper($this->primaryLang()) . ')',
            'keywords' => 'Keywords' . ' (' . strtoupper($this->primaryLang()) . ')',
            'country' => 'Country',
        );

        foreach ($this->languages() as $l) {
            $labels['title_' . $l] = Yii::t('admin', 'Title') . ' (' . strtoupper($l) . ')';
            $labels['description_' . $l] = Yii::t('admin', 'Description') . ' (' . strtoupper($l) . ')';
            $labels['keywords_' . $l] = Yii::t('admin', 'Keywords') . ' (' . strtoupper($l) . ')';
        }

        return $labels;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('keywords', $this->keywords, true);
        $criteria->compare('country', $this->country, true);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }

}
