<?php

/**
 * This is the model class for table "slider".
 *
 * The followings are the available columns in table 'slider':
 * @property integer $id
 * @property string $immagine
 * @property string $titolo
 * @property string $label1
 * @property string $label2
 * @property string $label3
 * @property string $label4
 * @property string $label5
 * @property string $label6
 * @property integer $pagina1
 * @property integer $pagina2
 * @property integer $pagina3
 * @property integer $pagina4
 * @property integer $pagina5
 * @property integer $pagina6
 * @property integer $visible
 * @property string $country
 * @property integer $ordine
 *
 * The followings are the available model relations:
 * @property Page $pagina10
 * @property Page $pagina20
 * @property Page $pagina30
 * @property Page $pagina40
 */
class Slider extends MultilingualActiveRecord {

    public function init() {
        $this->country = CountryManager::getCountryCode();

        parent::init();
    }

    //primary language
    public function primaryLang() {
        $country = Country::model()->findByPk(($this->country) ? $this->country : CountryManager::getCountryCode());

        return $country->primary_lang;
    }

    //additional languages found in the translations table
    public function languages() {
        if ($this->primaryLang() != "en") {
            return array("en");
        } else
            return array();
    }

    //attributes to look for in the translations table
    public function localizedAttributes() {
        return array('titolo', 'label1', 'label2', 'label3', 'label4', 'label5', 'label6');
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'slider';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        $rules = array(
            array('immagine', 'file',
                'types' => 'jpg, jpeg, gif, png',
                'allowEmpty' => false,
                'on' => 'insert'
            ),
            array('immagine', 'file',
                'types' => 'jpg, jpeg, gif, png',
                'allowEmpty' => true,
                'on' => 'update'
            ),
            array('titolo, visible, country, ordine', 'required'),
            array('pagina1, pagina2, pagina3, pagina4, pagina5, pagina6, visible, ordine', 'numerical', 'integerOnly' => true),
            array('immagine', 'length', 'max' => 256),
            array('titolo', 'length', 'max' => 128),
            array('label1, label2, label3, label4, label5, label6', 'length', 'max' => 100),
            array('country', 'length', 'max' => 2),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, immagine, titolo, label1, label2, label3, label4, label5, label6, pagina1, pagina2, pagina3, pagina4, pagina5, pagina6, visible, country, ordine', 'safe', 'on' => 'search'),
        );


        foreach ($this->languages() as $l) {
            $rules[] = array('titolo_' . $l, 'required');
            $rules[] = array('titolo_' . $l, 'length', 'max' => 128);
            $rules[] = array('titolo_' . $l, 'safe');

            $rules[] = array("label1_$l, label2_$l, label3_$l, label4_$l, label5_$l, label6_$l", 'length', 'max' => 100);
        }


        return $rules;
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'pagina10' => array(self::BELONGS_TO, 'Page', 'pagina1'),
            'pagina20' => array(self::BELONGS_TO, 'Page', 'pagina2'),
            'pagina30' => array(self::BELONGS_TO, 'Page', 'pagina3'),
            'pagina40' => array(self::BELONGS_TO, 'Page', 'pagina4'),
            'pagina50' => array(self::BELONGS_TO, 'Page', 'pagina5'),
            'pagina60' => array(self::BELONGS_TO, 'Page', 'pagina6'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        $labels = array(
            'id' => 'ID',
            'immagine' => Yii::t('admin', 'Immagine'),
            'titolo' => Yii::t('admin', 'Titolo').' (' . strtoupper($this->primaryLang()) . ')',
            'label1' => Yii::t('admin', 'Etichetta 1').' (' . strtoupper($this->primaryLang()) . ')',
            'label2' => Yii::t('admin', 'Etichetta 2').' (' . strtoupper($this->primaryLang()) . ')',
            'label3' => Yii::t('admin', 'Etichetta 3').' (' . strtoupper($this->primaryLang()) . ')',
            'label4' => Yii::t('admin', 'Etichetta 4').' (' . strtoupper($this->primaryLang()) . ')',
            'label5' => Yii::t('admin', 'Etichetta 5').' (' . strtoupper($this->primaryLang()) . ')',
            'label6' => Yii::t('admin', 'Etichetta 6').' (' . strtoupper($this->primaryLang()) . ')',
            'pagina1' => Yii::t('admin', 'Link etichetta 1'),
            'pagina2' => Yii::t('admin', 'Link etichetta 2'),
            'pagina3' => Yii::t('admin', 'Link etichetta 3'),
            'pagina4' => Yii::t('admin', 'Link etichetta 4'),
            'pagina5' => Yii::t('admin', 'Link etichetta 5'),
            'pagina6' => Yii::t('admin', 'Link etichetta 6'),
            'visible' => Yii::t('admin', 'Visibile'),
            'country' => Yii::t('admin', 'Nazione'),
        );


        foreach ($this->languages() as $l) {
            $labels['titolo_' . $l] = Yii::t('admin', 'Titolo').' (' . strtoupper($l) . ')';
            foreach (range(1, 6) as $num) {
                $labels['label' . $num . '_' . $l] = Yii::t('admin', 'Etichetta').' ' . $num . ' (' . strtoupper($l) . ')';
            }
        }

        return $labels;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('immagine', $this->immagine, true);
        $criteria->compare('titolo', $this->titolo, true);
        $criteria->compare('label1', $this->label1, true);
        $criteria->compare('label2', $this->label2, true);
        $criteria->compare('label3', $this->label3, true);
        $criteria->compare('label4', $this->label4, true);
        $criteria->compare('pagina1', $this->pagina1);
        $criteria->compare('pagina2', $this->pagina2);
        $criteria->compare('pagina3', $this->pagina3);
        $criteria->compare('pagina4', $this->pagina4);
        $criteria->compare('visible', $this->visible);
        $criteria->compare('country', $this->country, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchAdmin() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('immagine', $this->immagine, true);
        $criteria->compare('titolo', $this->titolo, true);
        $criteria->compare('label1', $this->label1, true);
        $criteria->compare('label2', $this->label2, true);
        $criteria->compare('label3', $this->label3, true);
        $criteria->compare('label4', $this->label4, true);
        $criteria->compare('pagina1', $this->pagina1);
        $criteria->compare('pagina2', $this->pagina2);
        $criteria->compare('pagina3', $this->pagina3);
        $criteria->compare('pagina4', $this->pagina4);
        $criteria->compare('visible', $this->visible);
        $criteria->compare('country', CountryManager::getCountryCode(), true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getNazione() {
        $country = Country::model()->findByPk($this->country);

        return $country->name;
    }

    public static function getSlides() {
        $criteria = new CDbCriteria;
        $criteria->order = "ordine";
        return self::model()->localized()->findAllByAttributes(array('country' => CountryManager::getCountryCode(), 'visible' => 1), $criteria);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Slider the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getImagesPath() {
        return Yii::app()->basePath . "/../uploads/slider/";
    }

    public function getImage() {
        return Yii::app()->baseUrl . "/uploads/slider/" . $this->immagine;
    }
    
    public function getPage($pageLabel){
        return Page::model()->localized()->findByPk($this->$pageLabel);
    }

}
