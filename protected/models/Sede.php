<?php

/**
 * This is the model class for table "sede".
 *
 * The followings are the available columns in table 'sede':
 * @property integer $id
 * @property string $titolo
 * @property string $immagine
 * @property string $telefono
 * @property string $fax
 * @property string $indirizzo
 * @property string $map
 * @property string $flag
 * @property string $nazione
 * @property integer $visibile
 * @property integer $ordine
 */
class Sede extends MultilingualActiveRecord {

    public static $THUMB_WIDTH = 300;
    public static $THUMB_HEIGHT = 120;
    public static $flags = array(
        "ita" => "Italia",
        "fra" => "France",
        "eng" => "United Kingdom",
        "deu" => "Deutschland",
        "rom" => "România",
        "spa" => "Spagna"
    );

    public function init() {
        $this->nazione = CountryManager::getCountryCode();

        parent::init();
    }

//primary language
    public function primaryLang() {
        $country = Country::model()->findByPk(($this->nazione) ? $this->nazione : CountryManager::getCountryCode());

        return $country->primary_lang;
    }

//additional languages found in the translations table
    public function languages() {
        if ($this->primaryLang() != "en") {
            return array("en");
        } else
            return array();
    }

//attributes to look for in the translations table
    public function localizedAttributes() {
        return array('titolo', 'indirizzo');
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'sede';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        $rules = array(
            array('titolo, immagine, telefono, indirizzo, map, flag, nazione', 'required'),
            array('visibile, ordine', 'numerical', 'integerOnly' => true),
            array('titolo', 'length', 'max' => 128),
            array('immagine', 'length', 'max' => 255),
            array('telefono, fax', 'length', 'max' => 32),
            array('indirizzo', 'length', 'max' => 400),
            array('flag', 'length', 'max' => 3),
            array('nazione', 'length', 'max' => 2),
            array('immagine', 'file',
                'types' => 'jpg, jpeg, gif, png',
                'allowEmpty' => false,
                'on' => 'insert'
            ),
            array('immagine', 'file',
                'types' => 'jpg, jpeg, gif, png',
                'allowEmpty' => true,
                'on' => 'update'
            ),
            // The following rule is used by search().
// @todo Please remove those attributes that should not be searched.
            array('id, titolo, immagine, telefono, fax, indirizzo, map, flag, nazione, visibile, ordine', 'safe', 'on' => 'search'),
        );

        foreach ($this->languages() as $l) {
            $rules[] = array("titolo_$l, indirizzo_$l", 'required');
            $rules[] = array("titolo_$l", 'length', 'max' => 128);
            $rules[] = array("indirizzo_$l", 'length', 'max' => 400);

            $rules[] = array("titolo_$l, indirizzo_$l", 'safe');
        }

        return $rules;
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        $labels = array(
            'id' => 'ID',
            'titolo' => Yii::t('admin', 'Titolo').' (' . strtoupper($this->primaryLang()) . ')',
            'immagine' => Yii::t('admin', 'Immagine'),
            'telefono' => Yii::t('admin', 'Telefono'),
            'fax' => Yii::t('admin', 'Fax'),
            'indirizzo' => Yii::t('admin', 'Indirizzo').' (' . strtoupper($this->primaryLang()) . ')',
            'map' => Yii::t('admin', 'Map'),
            'flag' => Yii::t('admin', 'Flag'),
            'nazione' => Yii::t('admin', 'Nazione'),
            'visibile' => Yii::t('admin', 'Visibile'),
            'ordine' =>  Yii::t('admin', 'Ordine'),
        );

        foreach ($this->languages() as $l) {
            $labels['titolo_' . $l] = Yii::t('admin', 'Titolo').' (' . strtoupper($l) . ')';
            $labels['indirizzo_' . $l] = Yii::t('admin', 'Indirizzo').' (' . strtoupper($l) . ')';
        }

        return $labels;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('titolo', $this->titolo, true);
        $criteria->compare('immagine', $this->immagine, true);
        $criteria->compare('telefono', $this->telefono, true);
        $criteria->compare('fax', $this->fax, true);
        $criteria->compare('indirizzo', $this->indirizzo, true);
        $criteria->compare('map', $this->map, true);
        $criteria->compare('flag', $this->flag, true);
        $criteria->compare('nazione', $this->nazione, true);
        $criteria->compare('visibile', $this->visibile);
        $criteria->compare('ordine', $this->ordine);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchAdmin() {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('titolo', $this->titolo, true);
        $criteria->compare('immagine', $this->immagine, true);
        $criteria->compare('telefono', $this->telefono, true);
        $criteria->compare('fax', $this->fax, true);
        $criteria->compare('indirizzo', $this->indirizzo, true);
        $criteria->compare('map', $this->map, true);
        $criteria->compare('flag', $this->flag, true);
        $criteria->compare('nazione', CountryManager::getCountryCode());
        $criteria->compare('visibile', $this->visibile);
        $criteria->compare('ordine', $this->ordine);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'ordine'
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Sede the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getImagesPath() {
        return Yii::app()->basePath . "/../uploads/images/";
    }

    public function getImage() {
    	return "http://www.geri.it/uploads/images/" . $this->immagine;
    //    return Yii::app()->baseUrl . "/uploads/images/" . $this->immagine;
    }

    public static function getImagesThumbPath() {
        return Yii::app()->basePath . "/../uploads/images/thumbs/";
    }

    public function getImageThumb() {
        //return Yii::app()->baseUrl . "/uploads/images/thumbs/" . $this->immagine;
        return "http://www.geri.it/uploads/images/". $this->immagine;
    }

    public static function getSedi() {
        return self::model()->localized()->findAllByAttributes(array('nazione' => CountryManager::getCountryCode(), 'visibile' => 1), array('order' => 'ordine'));
    }


}
