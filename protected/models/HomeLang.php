<?php

/**
 * This is the model class for table "homeLang".
 *
 * The followings are the available columns in table 'homeLang':
 * @property integer $homeId
 * @property string $lang
 * @property string $title
 * @property string $description
 * @property string $keywords
 */
class HomeLang extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return HomeLang the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'homeLang';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('homeId, lang, title, description, keywords', 'required'),
			array('homeId', 'numerical', 'integerOnly'=>true),
			array('lang', 'length', 'max'=>2),
			array('title, description, keywords', 'length', 'max'=>250),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('homeId, lang, title, description, keywords', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'homeId' => 'Home',
			'lang' => 'Lang',
			'title' => 'Title',
			'description' => 'Description',
			'keywords' => 'Keywords',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('homeId',$this->homeId);
		$criteria->compare('lang',$this->lang,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('keywords',$this->keywords,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}