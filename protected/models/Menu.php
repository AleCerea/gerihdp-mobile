<?php

/**
 * This is the model class for table "menu".
 *
 * The followings are the available columns in table 'menu':
 * @property integer $id
 * @property integer $static
 * @property string $static_type
 * @property integer $visible
 * @property string $label
 * @property string $country
 * @property integer $ordine
 * @property string $url
 *
 * The followings are the available model relations:
 * @property Country $country0
 * @property MenuLang[] $menuLangs
 */
class Menu extends MultilingualActiveRecord {

    public function init() {
        $this->country = CountryManager::getCountryCode();

        parent::init();
    }

//primary language
    public function primaryLang() {
        $country = Country::model()->findByPk(($this->country) ? $this->country : CountryManager::getCountryCode());

        return $country->primary_lang;
    }

//additional languages found in the translations table
    public function languages() {
        if ($this->primaryLang() != "en") {
            return array("en");
        } else
            return array();
    }

//attributes to look for in the translations table
    public function localizedAttributes() {
        return array('label', 'url');
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'menu';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        $rules = array(
            array('label, url, country, static', 'required'),
            array('visible, ordine', 'numerical', 'integerOnly' => true),
            array('label', 'length', 'max' => 128),
            array('url', 'length', 'max' => 256),
            array('static_type', 'countryUnique'),
            array('static_type', 'length', 'max' => 30),
            array('url', 'urlUnique'),
            array('url', 'match', 'pattern' => '/^([a-zA-Z0-9-_])*$/'),
            array('country', 'length', 'max' => 2),
            // The following rule is used by search().
// @todo Please remove those attributes that should not be searched.
            array('id, visible, label, country, ordine, url, static, static_type', 'safe', 'on' => 'search'),
        );

        foreach ($this->languages() as $l) {
            $rules[] = array('label_' . $l, 'required');
            $rules[] = array('label_' . $l, 'length', 'max' => 128);
            $rules[] = array('label_' . $l, 'safe');

            $rules[] = array('url_' . $l, 'required');
            $rules[] = array('url_' . $l, 'length', 'max' => 256);
            $rules[] = array('url_' . $l, 'urlUnique', 'lang' => $l);
            $rules[] = array('url_' . $l, 'safe');
            $rules[] = array('url_' . $l, 'match', 'pattern' => '/^([a-zA-Z0-9-_])*$/');
        }

        return $rules;
    }

    public function urlUnique($attribute, $params) {
        $attributeValue = $this->$attribute;
        $idWhere = ($this->isNewRecord) ? "" : "AND id != $this->id";

        if (isset($params['lang'])) {
            $lang = $params['lang'];
            $exists = Menu::model()->multilingual()->exists("multilingual.url = '$attributeValue' AND multilingual.lang = '$lang' $idWhere AND country = '$this->country'");
        } else {
            $exists = Menu::model()->multilingual()->exists("t.$attribute = '$attributeValue' $idWhere AND country = '$this->country'");
        }

        if ($exists) {
            $labels = $this->attributeLabels();
            $this->addError($attribute, $labels[$attribute] . " già in uso");
        }
    }

    public function countryUnique($attribute, $params) {
        $attributeValue = $this->$attribute;
        $idWhere = ($this->isNewRecord) ? "" : "AND id != $this->id";

        $exists = Menu::model()->multilingual()->exists("t.$attribute = '$attributeValue' $idWhere AND country = '$this->country'");

        if ($exists) {
            $labels = $this->attributeLabels();
            $this->addError($attribute, $labels[$attribute] . " già in uso");
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'country0' => array(self::BELONGS_TO, 'Country', 'country'),
            'menuLangs' => array(self::HAS_MANY, 'MenuLang', 'menuId'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        $labels = array(
            'id' => 'ID',
            'visible' => Yii::t('admin', 'Visibile'),
            'label' => Yii::t('admin', 'Nome') . ' (' . strtoupper($this->primaryLang()) . ')',
            'country' => Yii::t('admin', 'Nazione'),
            'ordine' => Yii::t('admin', 'Ordine'),
            'url' => Yii::t('admin', 'URL') . ' (' . strtoupper($this->primaryLang()) . ')',
            'static' => Yii::t('admin', 'Statica'),
            'static_type' => Yii::t('admin', 'Tipo statico')
        );

        foreach ($this->languages() as $l) {
            $labels['label_' . $l] = Yii::t('admin', 'Nome') . ' (' . strtoupper($l) . ')';
            $labels['url_' . $l] = Yii::t('admin', 'URL') . ' (' . strtoupper($l) . ')';
        }

        return $labels;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('visible', $this->visible);
        $criteria->compare('label', $this->label, true);
        $criteria->compare('country', $this->country, true);
        $criteria->compare('ordine', $this->ordine);
        $criteria->compare('url', $this->url, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchAdmin() {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('visible', $this->visible);
        $criteria->compare('label', $this->label, true);
        $criteria->compare('country', CountryManager::getCountryCode());
        $criteria->compare('ordine', $this->ordine);
        $criteria->compare('url', $this->url, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'ordine'
            )
        ));
    }

    public static function getItems() {
        $orderCriteria = new CDbCriteria;
        $orderCriteria->order = "ordine ASC";

        return self::model()->localized()->findAllByAttributes(array('country' => CountryManager::getCountry()->code, 'visible' => 1), $orderCriteria);
    }

    public function getPages() {
        $orderCriteria = new CDbCriteria;
        $orderCriteria->order = "ordine ASC";

        return Page::model()->localized()->findAllByAttributes(array('parent_menu_id' => $this->id, 'visible' => 1, 'country' => CountryManager::getCountry()->code), $orderCriteria);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Menu the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getNazione() {
        $country = Country::model()->findByPk($this->country);

        return $country->name;
    }

    public static function getVociMenu() {
        $criteria = new CDbCriteria;
        $criteria->condition = "country = '" . CountryManager::getCountryCode() . "' AND static != 1";
        $criteria->order = "ordine";

        return CHtml::listData(self::model()->findAll($criteria), "id", "label");
    }

    public static function getVoceByStaticType($type) {
        return self::model()->localized()->findByAttributes(array('static' => 1, 'static_type' => $type, 'country' => CountryManager::getCountryCode()));
    }

}
