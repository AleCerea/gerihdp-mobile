<?php

/**
 * This is the model class for table "Config".
 *
 * The followings are the available columns in table 'Config':
 * @property integer $id
 * @property string $valore
 * @property string $country
 * @property integer $codice
 *
 * The followings are the available model relations:
 * @property ConfigValues $id0
 * @property ConfigLang[] $configLangs
 */
class Config extends MultilingualActiveRecord {

    public $key;

    public function init() {
        $this->country = CountryManager::getCountryCode();

        parent::init();
    }

//primary language
    public function primaryLang() {
        $country = Country::model()->findByPk(($this->country) ? $this->country : CountryManager::getCountryCode());

        return $country->primary_lang;
    }

//additional languages found in the translations table
    public function languages() {
        if ($this->primaryLang() != "en") {
            return array("en");
        } else
            return array();
    }

//attributes to look for in the translations table
    public function localizedAttributes() {
        return array('valore');
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'config';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        $rules = array(
            array('codice, valore, country', 'required'),
            array('codice', 'unique', 'message' => 'Il valore è stato già creato', 'on' => 'insert'),
            array('valore', 'length', 'max' => 255),
            array('country', 'length', 'max' => 2),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, valore, country, codice', 'safe', 'on' => 'search'),
        );

        foreach ($this->languages() as $l) {
            $rules[] = array('valore_' . $l, 'required');
            $rules[] = array('valore_' . $l, 'length', 'max' => 255);
            $rules[] = array('valore_' . $l, 'safe');
        }

        return $rules;
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'value' => array(self::BELONGS_TO, 'ConfigValues', 'codice'),
            'configLangs' => array(self::HAS_MANY, 'ConfigLang', 'idConfig'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        $labels = array(
            'id' => Yii::t('admin', 'Tipo'),
            'valore' => Yii::t('admin', 'Valore') . ' (' . strtoupper($this->primaryLang()) . ')',
            'country' => 'Country',
        );

        foreach ($this->languages() as $l) {
            $labels['valore_' . $l] = Yii::t('admin', 'Valore ') . ' (' . strtoupper($l) . ')';
        }

        return $labels;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('valore', $this->valore, true);
        $criteria->compare('value.key', $this->key, true);
        $criteria->compare('country', $this->country, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchAdmin() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('valore', $this->valore, true);
        $criteria->compare('value.key', $this->key, true);
        $criteria->compare('country', CountryManager::getCountryCode());

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Config the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getNazione() {
        $country = Country::model()->findByPk($this->country);

        return $country->name;
    }

    public static function getConfigByKey($key) {
        $country = CountryManager::getCountryCode();
        $value = self::model()->localized()->with(array("value" => array("condition" => "value.key = '$key' AND country = '$country'")))->find();

        if ($value !== null) {
            return $value->valore;
        } else
            return "";
    }

}
