<?php

/**
 * This is the model class for table "faq".
 *
 * The followings are the available columns in table 'faq':
 * @property integer $id
 * @property string $domanda
 * @property string $risposta
 * @property string $country
 */
class Faq extends MultilingualActiveRecord {

    public function init() {
        $this->country = CountryManager::getCountryCode();

        parent::init();
    }

    //primary language
    public function primaryLang() {
        $country = Country::model()->findByPk(($this->country) ? $this->country : CountryManager::getCountryCode());

        return $country->primary_lang;
    }

    //additional languages found in the translations table
    public function languages() {
        if ($this->primaryLang() != "en") {
            return array("en");
        } else
            return array();
    }

    //attributes to look for in the translations table
    public function localizedAttributes() {
        return array('domanda', 'risposta');
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'faq';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        $rules = array(
            array('domanda, risposta, country', 'required'),
            array('domanda', 'length', 'max' => 255),
            array('risposta', 'length', 'max' => 500),
            array('country', 'length', 'max' => 2),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, domanda, risposta, country', 'safe', 'on' => 'search'),
        );

        foreach ($this->languages() as $lang) {
            $rules[] = array("domanda_$lang, risposta_$lang", 'required');
            $rules[] = array("domanda_$lang, risposta_$lang", 'safe');
        }

        return $rules;
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        $labels = array(
            'id' => 'ID',
            'domanda' => Yii::t('admin', 'Domanda') . ' (' . strtoupper($this->primaryLang()) . ')',
            'risposta' => Yii::t('admin', 'Risposta') . ' (' . strtoupper($this->primaryLang()) . ')',
            'country' => 'Country',
        );

        foreach ($this->languages() as $lang) {
            $labels["domanda_$lang"] = Yii::t('admin', 'Domanda') . ' (' . strtoupper($lang) . ')';
            $labels["risposta_$lang"] = Yii::t('admin', 'Risposta') . ' (' . strtoupper($lang) . ')';
        }

        return $labels;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('domanda', $this->domanda, true);
        $criteria->compare('risposta', $this->risposta, true);
        $criteria->compare('country', $this->country, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchAdmin() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('domanda', $this->domanda, true);
        $criteria->compare('risposta', $this->risposta, true);
        $criteria->compare('country', CountryManager::getCountryCode(), true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Faq the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
