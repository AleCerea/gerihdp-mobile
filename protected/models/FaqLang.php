<?php

/**
 * This is the model class for table "faqLang".
 *
 * The followings are the available columns in table 'faqLang':
 * @property integer $faqId
 * @property string $lang
 * @property string $domanda
 * @property string $risposta
 */
class FaqLang extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'faqLang';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('faqId, lang, domanda, risposta', 'required'),
			array('faqId', 'numerical', 'integerOnly'=>true),
			array('lang', 'length', 'max'=>2),
			array('domanda', 'length', 'max'=>255),
			array('risposta', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('faqId, lang, domanda, risposta', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'faqId' => 'Faq',
			'lang' => 'Lang',
			'domanda' => 'Domanda',
			'risposta' => 'Risposta',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('faqId',$this->faqId);
		$criteria->compare('lang',$this->lang,true);
		$criteria->compare('domanda',$this->domanda,true);
		$criteria->compare('risposta',$this->risposta,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FaqLang the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
