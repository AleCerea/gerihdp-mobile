<?php

/**
 * This is the model class for table "block_template".
 *
 * The followings are the available columns in table 'block_template':
 * @property integer $id
 * @property string $html
 * @property string $miniatura
 * @property integer $ordine
 */
class BlockTemplate extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'block_template';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('html', 'required'),
            array('miniatura', 'length', 'max' => 256),
            array('miniatura', 'file',
                'types' => 'jpg, jpeg, gif, png',
                'allowEmpty' => false,
                'on' => 'insert'
            ),
            array('miniatura', 'file',
                'types' => 'jpg, jpeg, gif, png',
                'allowEmpty' => true,
                'on' => 'update'
            ),
            array('ordine', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, html, miniatura, ordine', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'html' => Yii::t('admin', 'Html'),
            'miniatura' => Yii::t('admin', 'Miniatura'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('html', $this->html, true);
        $criteria->compare('miniatura', $this->miniatura, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 20
            ),
            'sort' => array(
                'defaultOrder' => 'ordine'
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return BlockTemplate the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getMiniaturePath() {
        return Yii::app()->basePath . "/../uploads/templates/";
    }

    public function getMiniaturaUrl() {
        return Yii::app()->baseUrl . "/uploads/templates/" . $this->miniatura;
    }

    public function getMiniaturaImg() {
        return "<img src='" . $this->getMiniaturaUrl() . "' alt='$this->miniatura' />";
    }

}
