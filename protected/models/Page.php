<?php

/**
 * This is the model class for table "page".
 *
 * The followings are the available columns in table 'page':
 * @property integer $id
 * @property integer $parent_menu_id
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $url
 * @property integer $visible
 * @property string $country
 * @property integer $ordine
 * @property string $creation_date
 * @property string $static
 * @property string $static_type
 *
 * The followings are the available model relations:
 * @property Menu $parentMenu
 * @property PageLang[] $pageLangs
 */
class Page extends MultilingualActiveRecord {

    public function init() {
        $this->country = CountryManager::getCountryCode();

        parent::init();
    }

    //primary language
    public function primaryLang() {
        $country = Country::model()->findByPk(($this->country) ? $this->country : CountryManager::getCountryCode());

        return $country->primary_lang;
    }

    //additional languages found in the translations table
    public function languages() {
        if ($this->primaryLang() != "en") {
            return array("en");
        } else
            return array();
    }

    //attributes to look for in the translations table
    public function localizedAttributes() {
        return array('title', 'description', 'keywords', 'url');
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'page';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        $rules = array(
            array('parent_menu_id, title, description, keywords, url, country, creation_date', 'required'),
            array('parent_menu_id, visible, ordine, static', 'numerical', 'integerOnly' => true),
            array('title', 'length', 'max' => 128),
            array('static_type', 'length', 'max' => 32),
            array('description, keywords, url', 'length', 'max' => 255),
            array('url', 'match', 'pattern' => '/^([a-zA-Z0-9-_])*$/'),
            array('url', 'urlUnique'),
            array('country', 'length', 'max' => 2),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, parent_menu_id, title, description, keywords, url, visible, country, ordine, creation_date, static, static_type', 'safe', 'on' => 'search'),
        );

        foreach ($this->languages() as $l) {
            $rules[] = array("title_$l, description_$l, keywords_$l, url_$l", 'required');
            $rules[] = array('url_' . $l, 'match', 'pattern' => '/^([a-zA-Z0-9-_])*$/');
            $rules[] = array('url_' . $l, 'urlUnique', 'lang' => $l);

            $rules[] = array("title_$l", 'length', 'max' => 128);
            $rules[] = array("description_$l, keywords_$l, url_$l", 'length', 'max' => 255);
        }


        return $rules;
    }

    public function urlUnique($attribute, $params) {
        $attributeValue = $this->$attribute;
        $idWhere = ($this->isNewRecord) ? "" : "AND id != $this->id";

        if (isset($params['lang'])) {
            $lang = $params['lang'];
            $exists = Page::model()->multilingual()->exists("multilingual.url = '$attributeValue' AND multilingual.lang = '$lang' $idWhere AND country = '$this->country' AND parent_menu_id = $this->parent_menu_id");
        } else {
            $exists = Page::model()->multilingual()->exists("t.$attribute = '$attributeValue' $idWhere AND country = '$this->country' AND parent_menu_id = $this->parent_menu_id");
        }

        if ($exists) {
            $labels = $this->attributeLabels();
            $this->addError($attribute, $labels[$attribute] . " già in uso");
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'parentMenu' => array(self::BELONGS_TO, 'Menu', 'parent_menu_id'),
            'pageLangs' => array(self::HAS_MANY, 'PageLang', 'pageId'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        $labels = array(
            'id' => 'ID',
            'parent_menu_id' => Yii::t('admin', 'Voce di Menu'),
            'title' => Yii::t('admin', 'Titolo').' (' . strtoupper($this->primaryLang()) . ')',
            'description' => Yii::t('admin', 'Descrizione').' (' . strtoupper($this->primaryLang()) . ')',
            'keywords' => Yii::t('admin', 'Keywords').' (' . strtoupper($this->primaryLang()) . ')',
            'url' => Yii::t('admin', 'URL').' (' . strtoupper($this->primaryLang()) . ')',
            'visible' => Yii::t('admin', 'Visibile'),
            'country' => Yii::t('admin', 'Nazione'),
            'ordine' => Yii::t('admin', 'Ordine'),
            'creation_date' => Yii::t('admin', 'Data creazione'),
            'static' => Yii::t('admin', 'Statica'),
            'static_type' => Yii::t('admin', 'Tipo statico'),
        );

        foreach ($this->languages() as $l) {
            $labels['title_' . $l] = Yii::t('admin', 'Titolo').' (' . strtoupper($l) . ')';
            $labels['description_' . $l] = Yii::t('admin', 'Descrizione').' (' . strtoupper($l) . ')';
            $labels['keywords_' . $l] = Yii::t('admin', 'Keywords').' (' . strtoupper($l) . ')';
            $labels['url_' . $l] = Yii::t('admin', 'URL').' (' . strtoupper($l) . ')';
        }

        return $labels;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('parent_menu_id', $this->parent_menu_id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('keywords', $this->keywords, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('visible', $this->visible);
        $criteria->compare('country', $this->country, true);
        $criteria->compare('creation_date', $this->creation_date, true);
        $criteria->compare('ordine', $this->ordine);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchAdmin() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->with = "parentMenu";

        $criteria->compare('id', $this->id);
        $criteria->compare('parent_menu_id', $this->parent_menu_id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('keywords', $this->keywords, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('visible', $this->visible);
        $criteria->compare('t.country', CountryManager::getCountryCode(), true);
        $criteria->compare('creation_date', $this->creation_date, true);
        $criteria->compare('t.ordine', $this->ordine);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'parentMenu.ordine, t.ordine'
            )
        ));
    }

    public function getBlocks() {
        $criteria = new CDbCriteria;
        $criteria->order = "ordine ASC";
                
        return Block::model()->localized()->findAllByAttributes(array('page_id' => $this->id, 'country' => CountryManager::getCountryCode(), 'visible' => 1), $criteria);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Page the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getVoceMenu() {
        $voceMenu = Menu::model()->multilingual()->findByPk($this->parent_menu_id);
        if ($voceMenu !== null) {
            return $voceMenu->label;
        } else {
            return "";
        }
    }

    public function getNameforlink() {
        return $this->parentMenu->url . "/" . $this->url;
    }

    public static function getForLink() {
        $criteria = new CDbCriteria;
        $criteria->order = "t.parent_menu_id, t.id";

        return CHtml::listData(self::model()->with("parentMenu")->findAllByAttributes(array('country' => CountryManager::getCountryCode()), $criteria), "id", "nameforlink");
    }

    public function getParentMenu() {
        return Menu::model()->localized()->findByPk($this->parent_menu_id);
    }

    public function getMenuFilter() {
        return CHtml::dropDownList("Page[parent_menu_id]", $this->parent_menu_id, CHtml::listData(Menu::model()->multilingual()->findAllByAttributes(array('country' => CountryManager::getCountryCode()), array('order' => 't.label')), "id", "label"), array('empty' => ''));
    }

}
