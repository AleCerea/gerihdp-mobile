<?php

/**
 * This is the model class for table "block".
 *
 * The followings are the available columns in table 'block':
 * @property integer $id
 * @property integer $page_id
 * @property integer $ordine
 * @property string $url
 * @property string $title
 * @property string $description
 * @property string $country
 * @property string $creation_date
 * @property string $bgcolor
 * @property integer $visible
 *
 * The followings are the available model relations:
 * @property Country $country0
 * @property Page $page
 */
class Block extends MultilingualActiveRecord {

    public function init() {
        $this->country = CountryManager::getCountryCode();

        parent::init();
    }

    //primary language
    public function primaryLang() {
        $country = Country::model()->findByPk(($this->country) ? $this->country : CountryManager::getCountryCode());

        return $country->primary_lang;
    }

    //additional languages found in the translations table
    public function languages() {
        if ($this->primaryLang() != "en") {
            return array("en");
        } else
            return array();
    }

    //attributes to look for in the translations table
    public function localizedAttributes() {
        return array('url', 'title', 'description');
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'block';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        $rules = array(
            array('page_id, url, title, description, country, creation_date', 'required'),
            array('page_id, ordine, visible', 'numerical', 'integerOnly' => true),
            array('url, title', 'length', 'max' => 256),
            array('country', 'length', 'max' => 2),
            array('url', 'match', 'pattern' => '/^([a-zA-Z0-9-_])*$/'),
            array('url', 'urlUnique'),
            array('bgcolor', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, page_id, ordine, url, title, description, country, creation_date, bgcolor, visible', 'safe', 'on' => 'search'),
        );

        foreach ($this->languages() as $l) {
            $rules[] = array('url_' . $l, 'required');
            $rules[] = array('url_' . $l, 'length', 'max' => 256);
            $rules[] = array('url_' . $l, 'match', 'pattern' => '/^([a-zA-Z0-9-_])*$/');
            $rules[] = array('url_' . $l, 'urlUnique', 'lang' => $l);
            $rules[] = array('url_' . $l, 'safe');

            $rules[] = array('title_' . $l, 'required');
            $rules[] = array('title_' . $l, 'length', 'max' => 256);
            $rules[] = array('title_' . $l, 'safe');

            $rules[] = array('description_' . $l, 'required');
            $rules[] = array('description_' . $l, 'safe');
        }

        return $rules;
    }

    public function urlUnique($attribute, $params) {
        $attributeValue = $this->$attribute;
        $idWhere = ($this->isNewRecord) ? "" : "AND id != $this->id";

        if (isset($params['lang'])) {
            $lang = $params['lang'];
            $exists = Block::model()->multilingual()->exists("multilingual.url = '$attributeValue' AND multilingual.lang = '$lang' $idWhere AND country = '$this->country' AND page_id = $this->page_id");
        } else {
            $exists = Block::model()->multilingual()->exists("t.$attribute = '$attributeValue' $idWhere AND country = '$this->country' AND page_id = $this->page_id");
        }

        if ($exists) {
            $labels = $this->attributeLabels();
            $this->addError($attribute, $labels[$attribute] . " già in uso");
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'country0' => array(self::BELONGS_TO, 'Country', 'country'),
            'page' => array(self::BELONGS_TO, 'Page', 'page_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        $labels = array(
            'id' => 'ID',
            'page_id' => 'Page',
            'ordine' => Yii::t('admin', 'Ordine'),
            'url' => Yii::t('admin', 'URL') . ' (' . strtoupper($this->primaryLang()) . ')',
            'title' => Yii::t('admin', 'Titolo') . ' (' . strtoupper($this->primaryLang()) . ')',
            'description' => Yii::t('admin', 'Descrizione') . ' (' . strtoupper($this->primaryLang()) . ')',
            'country' => Yii::t('admin', 'Nazione'),
            'creation_date' => Yii::t('admin', 'Data creazione'),
            'bgcolor' => Yii::t('admin', 'Colore di sfondo'),
            'visible' => Yii::t('admin', 'Visibile')
        );

        foreach ($this->languages() as $l) {
            $labels['title_' . $l] = Yii::t('admin', 'Titolo') . ' (' . strtoupper($l) . ')';
            $labels['description_' . $l] = Yii::t('admin', 'Descrizione').' (' . strtoupper($l) . ')';
            $labels['url_' . $l] = Yii::t('admin', 'URL').' (' . strtoupper($l) . ')';
        }

        return $labels;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('page_id', $this->page_id);
        $criteria->compare('ordine', $this->ordine);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('country', $this->country, true);
        $criteria->compare('creation_date', $this->creation_date, true);
        $criteria->compare('visible', $this->visible);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getNazione() {
        $country = Country::model()->findByPk($this->country);

        return $country->name;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Block the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
