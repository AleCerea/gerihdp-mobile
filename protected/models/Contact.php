<?php

/**
 * This is the model class for table "contact".
 *
 * The followings are the available columns in table 'contact':
 * @property integer $id
 * @property string $nome
 * @property string $cognome
 * @property string $email
 * @property string $societa
 * @property string $telefono
 * @property string $messaggio
 * @property string $data_inserimento
 * @property string $nazione
 * @property string $lingua
 */
class Contact extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contact';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nome, cognome, email, data_inserimento, nazione, lingua', 'required'),
			array('nome, cognome, email, societa', 'length', 'max'=>255),
			array('telefono', 'length', 'max'=>32),
			array('nazione, lingua', 'length', 'max'=>2),
			array('messaggio', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nome, cognome, email, societa, telefono, messaggio, data_inserimento, nazione, lingua', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nome' => Yii::t('admin', 'Nome'),
			'cognome' => Yii::t('admin', 'Cognome'),
			'email' => Yii::t('admin', 'Email'),
			'societa' => Yii::t('admin', 'Societa'),
			'telefono' => Yii::t('admin', 'Telefono'),
			'messaggio' => Yii::t('admin', 'Messaggio'),
			'data_inserimento' => Yii::t('admin', 'Data Inserimento'),
			'nazione' => Yii::t('admin', 'Nazione'),
			'lingua' => Yii::t('admin', 'Lingua'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('cognome',$this->cognome,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('societa',$this->societa,true);
		$criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('messaggio',$this->messaggio,true);
		$criteria->compare('data_inserimento',$this->data_inserimento,true);
		$criteria->compare('nazione',$this->nazione,true);
		$criteria->compare('lingua',$this->lingua,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Contact the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
