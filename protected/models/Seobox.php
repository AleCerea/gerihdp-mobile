<?php

/**
 * This is the model class for table "seobox".
 *
 * The followings are the available columns in table 'seobox':
 * @property integer $id
 * @property string $titolo
 * @property string $testo
 * @property string $image_alt
 * @property string $immagine
 * @property string $nazione
 * @property integer $visibile
 * @property string $box
 * @property integer $link
 * @property string $link_esterno
 * 
 * * The followings are the available model relations:
 * @property TextboxLang[] $textboxLangs
 * @property Page $pageLink
 */
class Seobox extends MultilingualActiveRecord {

    public function init() {
        $this->nazione = CountryManager::getCountryCode();

        parent::init();
    }

    public function primaryLang() {
        $country = Country::model()->findByPk(($this->nazione) ? $this->nazione : CountryManager::getCountryCode());

        return $country->primary_lang;
    }

    public function languages() {
        if ($this->primaryLang() != "en") {
            return array("en");
        } else
            return array();
    }

    public function localizedAttributes() {
        return array('titolo', 'testo', 'image_alt');
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'seobox';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        $rules = array(
            array('titolo, nazione, box', 'required'),
            array('visibile, link', 'numerical', 'integerOnly' => true),
            array('titolo, image_alt', 'length', 'max' => 255),
            array('immagine', 'length', 'max' => 256),
            array('nazione', 'length', 'max' => 2),
            array('box', 'length', 'max' => 4),
            array('link_esterno', 'length', 'max' => 300),
            array('testo', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, titolo, testo, immagine, nazione, visibile, box, link, link_esterno', 'safe', 'on' => 'search'),
        );

        foreach ($this->languages() as $l) {
            $rules[] = array("testo_$l, titolo_$l, image_alt_$l", 'safe');
        }

        return $rules;
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'textboxLangs' => array(self::HAS_MANY, 'TextboxLang', 'idTextbox'),
            'pageLink' => array(self::BELONGS_TO, 'Page', 'link'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        $labels = array(
            'id' => 'ID',
            'titolo' => 'Titolo',
            'testo' => 'Testo',
            'immagine' => 'Immagine',
            'nazione' => 'Nazione',
            'visibile' => 'Visibile',
            'box' => 'Box',
            'link' => 'Link',
            'link_esterno' => 'Link Esterno',
            'image_alt' => 'Testo alternativo immagine'
        );


        foreach ($this->languages() as $l) {
            $labels['testo_' . $l] = Yii::t('admin', 'Testo') . ' (' . strtoupper($l) . ')';
            $labels['titolo_' . $l] = Yii::t('admin', 'Titolo') . ' (' . strtoupper($l) . ')';
            $labels['image_alt_' . $l] = Yii::t('admin', 'Testo alternativo immagine') . ' (' . strtoupper($l) . ')';
        }

        return $labels;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('titolo', $this->titolo, true);
        $criteria->compare('testo', $this->testo, true);
        $criteria->compare('immagine', $this->immagine, true);
        $criteria->compare('nazione', $this->nazione, true);
        $criteria->compare('visibile', $this->visibile);
        $criteria->compare('box', $this->box, true);
        $criteria->compare('link', $this->link);
        $criteria->compare('link_esterno', $this->link_esterno, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchAdmin() {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('titolo', $this->titolo, true);
        $criteria->compare('testo', $this->testo, true);
        $criteria->compare('immagine', $this->immagine, true);
        $criteria->compare('nazione', CountryManager::getCountryCode());
        $criteria->compare('visibile', $this->visibile);
        $criteria->compare('box', $this->box, true);
        $criteria->compare('link', $this->link);


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'box'
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Seobox the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getImagesPath() {
        return Yii::app()->basePath . "/../uploads/highlights/";
    }

    public function getImage() {
        return "http://www.geri.it/uploads/highlights/" . $this->immagine;
    }

    public static function getBoxes() {
        return array(
            "box1" => Yii::t('admin', "Box 1"),
            "box2" => Yii::t('admin', "Box 2"),
            "box3" => Yii::t('admin', "Box 3"),
            "box4" => Yii::t('admin', "Box 4"),
        );
    }

    public static function getLayouts() {
        return array(
            "1" => Yii::t('admin', "Layout 1"),
            "2" => Yii::t('admin', "Layout 2"),
            "3" => Yii::t('admin', "Layout 3"),
        );
    }

    public function getImageTag($maxWidth) {
        if ($this->immagine != null && $this->immagine != "")
            return "<img src='" . $this->getImage() . "' alt='' style='max-width:{$maxWidth}px'>";
        else
            return "";
    }

    public function getBox() {
        $boxes = self::getBoxes();
        return $boxes[$this->box];
    }

    public function getPage() {
        return Page::model()->localized()->findByPk($this->link);
    }

    public function getURL() {
        if ($this->getPage() != null) {
            return Yii::app()->createUrl('home/page', array('country' => strtolower(CountryManager::getCountryCode()), 'menu' => $this->getPage()->getParentMenu()->url, 'page' => $this->getPage()->url));
        } else {
            return $this->link_esterno;
        }
    }

    public function getURLTarget() {
        if ($this->getPage() != null) {
            return "";
        } else {
            return "_blank";
        }
    }

}
