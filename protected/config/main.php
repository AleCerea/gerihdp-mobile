<?php

// uncomment the following to define a path alias
Yii::setPathOfAlias('local', 'path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'GERIHDP - mobile',
//    'baseUrl' => 'http://m.gerihdp.com',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.extensions.*',
        'bootstrap.helpers.TbHtml',
        'application.extensions.YiiMailer.YiiMailer',
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'geri2014',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
            'generatorPaths' => array('bootstrap.gii'),
        ),
        'admin',
        'faidate'
    ),
    'aliases' => array(
        'bootstrap' => realpath(__DIR__ . '/../extensions/bootstrap'), // change this if necessary
    ),
    // application components
    'components' => array(
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        'bootstrap' => array(
            'class' => 'bootstrap.components.TbApi',
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'class' => 'application.extensions.GeriUrlManager',
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                '' => 'site/index',
                'gii' => 'gii',
                'gii/<controller:\w+>/<action:\w+>' => 'gii/<controller>/<action>',
                'faidate' => 'faidate/default/index',
                'faidate/<controller:\w+>/<id:\d+>' => 'faidate/<controller>/view',
                'faidate/<controller:\w+>/<action:\w+>/<id:\d+>' => 'faidate/<controller>/<action>',
                'faidate/<controller:\w+>/<action:\w+>' => 'faidate/<controller>/<action>',
                'faidate/<controller:\w+>' => 'faidate/<controller>/index',
                'admin' => 'admin/default/index',
                'admin/<controller:\w+>/<id:\d+>' => 'admin/<controller>/view',
                'admin/<controller:\w+>/<action:\w+>/<id:\d+>' => 'admin/<controller>/<action>',
                'admin/<controller:\w+>/<action:\w+>' => 'admin/<controller>/<action>',
                'site/changeCountry' => 'site/changeCountry',
                'site/setCookies' => 'site/setCookies',
                'site/captcha/<v:\w+>' => 'site/captcha/',
                
//                '<country:(it|es|fr|ro|uk|de)>' => 'site/index',
//                '<country:(it|es|fr|ro|uk|de)>/<menu:[\w\-]++>/<page:\d+>/<slug>' => 'site/page',
//                '<country:(it|es|fr|ro|uk|de)>/<menu:[\w\-]++>/<page:[\w\-]++>' => 'site/page',
//                '<country:(it|es|fr|ro|uk|de)>/<menu:[\w\-]++>/<page:\d+>' => 'site/page',
//                '<country:(it|es|fr|ro|uk|de)>/<menu:[\w\-]++>/' => 'site/page',
//                '<country:(it|es|fr|ro|uk|de)>/<controller:\w+>/<id:\d+>' => '<controller>/view',
//                '<country:(it|es|fr|ro|uk|de)>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
//                '<country:(it|es|fr|ro|uk|de)>/<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                
                '<menu:[\w\-]++>/<page:\d+>/<slug>' => 'site/page',
                '<menu:[\w\-]++>/<page:[\w\-]++>' => 'site/page',
                '<menu:[\w\-]++>/<page:\d+>' => 'site/page',
                '<menu:[\w\-]++>/' => 'site/page',
                
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        // database settings are configured in database.php
        'db' => require(dirname(__FILE__) . '/database.php'),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'webmaster@example.com',
        'contact_email' => 'direzione.commerciale@gerihdp.it',
    ),
    'language' => 'it'
);
